package nz.ac.vuw.ecs.hazlehjack.graph.utils;

public class Regex {
    public static final String PACKAGE_REGEX = "<([\\w.]+)\\.[\\w$]+:";
    public static final String CLASS_REGEX = "\\.?([\\w$]+):";
    public static final String RETURN_REGEX = ": ([\\w.\\[\\]$]+) ";
    public static final String METHOD_REGEX = "([\\w<>$]+)\\(";
    public static final String PARAM_REGEX = "(?:,|\\()([^,\\)]+)";
    public static final String METHOD_SIGNATURE_REGEX = "<[\\w_\\.]*\\.?[\\w_\\$]+: [\\w_\\.\\[\\]\\$]+ [\\w_<>$]+\\([\\w_\\$\\.,\\[\\]]*\\)>";

    public static final String BC_RETURN_TYPE = "\\(.*\\)([VZCBSIFJD])|L([\\w\\/]*);";
    public static final String BC_ARGUMENTS = "\\((.*)\\)";
    public static final String BC_CLASS_ARGUMENT = "L([\\w\\/]+);";
}
