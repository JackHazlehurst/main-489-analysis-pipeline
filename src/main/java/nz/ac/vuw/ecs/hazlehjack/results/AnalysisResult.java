package nz.ac.vuw.ecs.hazlehjack.results;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.nio.file.Path;
import java.time.Duration;

public class AnalysisResult {
    public static final AnalysisResult emptyPair = new AnalysisResult(null, null, Duration.ZERO);
    public final Path resource;
    public final Graph<GraphMethod, DefaultEdge> result;
    public final Duration runtime;

    private AnalysisResult(Path resource, Graph<GraphMethod, DefaultEdge> result, Duration runtime) {
        this.resource = resource;
        this.result = result;
        this.runtime = runtime;
    }

    public static AnalysisResult of(Path resource, Graph<GraphMethod, DefaultEdge> result, Duration runtime) {
        return new AnalysisResult(resource, result, runtime);
    }

    public String toSummaryString() {
        return "AnalysisResult{" +
                "resource=" + resource +
                ", result=" + GraphAnalyser.of(result) +
                ", runtime=" + runtime +
                '}';
    }

    @Override
    public String toString() {
        return "AnalysisResult{" +
                "resource=" + resource +
                ", result=" + result +
                ", runtime=" + runtime +
                '}';
    }
}
