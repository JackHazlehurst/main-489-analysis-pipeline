package nz.ac.vuw.ecs.hazlehjack.graph.utils;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Bytecode {
    public static final String CLASS_DESCRIPTOR = "L";
    private static final Map<String, String> descriptors = Stream.of(new String[][] {
            { "V", "void" },
            { "Z", "boolean" },
            { "C", "char" },
            { "B", "byte" },
            { "S", "short" },
            { "I", "int" },
            { "F", "float" },
            { "J", "long" },
            { "D", "double" },
            { CLASS_DESCRIPTOR, "<class>" },
    }).collect(Collectors.toMap(k -> k[0], v -> v[1]));

    public static String getDescriptor(String descriptor) {
        return descriptors.get(descriptor);
    }

    public static boolean isPrimitiveDescriptor(String descriptor) {
        if (!CLASS_DESCRIPTOR.equals(descriptor) && descriptors.containsKey(descriptor)) {
            return true;
        }
        else {
            return false;
        }
    }
}
