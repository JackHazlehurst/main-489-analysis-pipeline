package nz.ac.vuw.ecs.hazlehjack.results;

import nz.ac.vuw.ecs.hazlehjack.Util;
import nz.ac.vuw.ecs.hazlehjack.graph.CallGraphParser;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.unreflector.StaticAnalysisMain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Results {
    private static final Logger LOGGER = LogManager.getLogger(Results.class);
    public static final List<VersionResult> versionResultList = new ArrayList<>();

    /**
     * Result records must be created from oldest to newest
     * @param commitHash
     */
    public static void createResultRecord(final String commitHash) {
        versionResultList.add(VersionResult.of(commitHash));
    }

    public static void clear() {
        versionResultList.clear();
    }

    public static void recordDynamicAnalysis(final String commitHash, final Supplier<Path> analysisSupplier) {
        LOGGER.info("Recording dynamic analysis for: {}", commitHash);

        long startTime = System.currentTimeMillis();
        Path trackedInvocations = analysisSupplier.get();
        Duration executionTime = Duration.ofMillis(System.currentTimeMillis() - startTime);

        VersionResult result = getVersionResultFromHash(commitHash);
        VersionResult newResult = new VersionResult(result.commitHash,
                AnalysisResult.of(trackedInvocations, null, executionTime),
                result.staticAnalysis,
                result.chaAnalysis,
                result.optionalTestSet);

        versionResultList.set(versionResultList.indexOf(result), newResult);
    }

    /**
     *
     * @param commitHash
     * @param analysisSupplier supplier is used so timing can be done internally
     */
    public static void recordDoopAnalysis(final String commitHash, final Supplier<StaticAnalysisMain> analysisSupplier) {
        LOGGER.info("Recording DOOP analysis");

        long startTime = System.currentTimeMillis();
        VersionResult result = getVersionResultFromHash(commitHash);
        StaticAnalysisMain staticAnalysisMain = analysisSupplier.get();
        Path callGraphEdgeCsv = Paths.get(staticAnalysisMain.SHARED.toString(), staticAnalysisMain.CALL_GRAPH_EDGE_CSV);
        Graph<GraphMethod, DefaultEdge> staticGraph = CallGraphParser.parse(callGraphEdgeCsv);
        Duration executionTime = Duration.ofMillis(System.currentTimeMillis() - startTime);

        VersionResult newResult = new VersionResult(result.commitHash,
                result.dynamicAnalysis,
                AnalysisResult.of(callGraphEdgeCsv, staticGraph, executionTime),
                result.chaAnalysis,
                Optional.of(staticAnalysisMain.testSet));
        versionResultList.set(versionResultList.indexOf(result), newResult);
    }

    public static void recordChaAnalysis(final String commitHash, final Path classFiles) {
        LOGGER.info("Recording CHA analysis");

        VersionResult result = getVersionResultFromHash(commitHash);
        Graph<GraphMethod, DefaultEdge> toBuildOn = getPreviousVersionResultOrEmpty(result, r -> r.chaAnalysis.result);

        // Only the first CHA will need to use the previous static graph, others will just use the previous CHA
        if (commitHashToIndex(commitHash) == 1) {
            toBuildOn = getPreviousVersionResultOrEmpty(result, r -> r.staticAnalysis.result);
        }
        toBuildOn = Util.cloneGraph(toBuildOn);

        long startTime = System.currentTimeMillis();
        Graph<GraphMethod, DefaultEdge> chaGraph = CallGraphParser.parseCha(toBuildOn, classFiles);
        Duration executionTime = Duration.ofMillis(System.currentTimeMillis() - startTime);

        VersionResult newResult = new VersionResult(result.commitHash,
                result.dynamicAnalysis,
                result.staticAnalysis,
                AnalysisResult.of(classFiles, chaGraph, executionTime),
                result.optionalTestSet);

        versionResultList.set(versionResultList.indexOf(result), newResult);
    }

    public static Graph<GraphMethod, DefaultEdge> getPreviousVersionResultOrEmpty(VersionResult versionResult, Function<VersionResult, Graph<GraphMethod, DefaultEdge>> toGet) {
        int index = -1;
        try {
            index = commitHashToIndex(versionResult.commitHash) - 1;
        }
        catch (IllegalArgumentException e) {
            LOGGER.warn("Record not present in Results: {}", versionResult.commitHash);
        }

        if (index < 0) {
            return new DefaultDirectedGraph<>(DefaultEdge.class);
        }

        VersionResult result = versionResultList.get(index);
        Graph<GraphMethod, DefaultEdge> previousGraph = toGet.apply(result);

        if (previousGraph == null) {
            return new DefaultDirectedGraph<>(DefaultEdge.class);
        }
        return previousGraph;
    }

    public static int commitHashToIndex(String commitHash) {
        for(int i = 0; i < versionResultList.size(); i++) {
            VersionResult versionResult = versionResultList.get(i);
            if (versionResult.commitHash.equals(commitHash)) {
                return i;
            }
        }
        throw new IllegalArgumentException(commitHash + " is not present in versionResultList");
    }

    public static VersionResult getVersionResultFromHash(String commitHash) {
        return versionResultList.get(commitHashToIndex(commitHash));
    }

    public static String toMetaDataString() {
        return versionResultList.stream()
                .map(v -> String.format("%s:\n\tDynamic Analysis: %s\n\tStatic Analysis: %s\n\tCHA Analysis: %s\n",
                        v.commitHash,
                        v.dynamicAnalysis.resource,
                        v.staticAnalysis.resource,
                        v.chaAnalysis.resource))
                .collect(Collectors.joining());
    }

    public static String toResultsString() {
        return versionResultList.stream()
                .map(VersionResult::toString)
                .collect(Collectors.joining("\n"));
    }

    public static String toTimes() {
        String timesString = "";

        for(int i = 0; i < versionResultList.size(); i++) {
            VersionResult versionResult = versionResultList.get(i);

            timesString += String.format("Dynamic-%d: %s\n", i, versionResult.dynamicAnalysis.runtime.toString());
            timesString += String.format("Static-%d: %s\n", i, versionResult.staticAnalysis.runtime.toString());
            timesString += String.format("CHA-%d: %s\n", i, versionResult.chaAnalysis.runtime.toString());
            timesString += "\n";
        }

        return timesString;
    }

    public static String toGraphString() {
        return "\nDynamic Graphs:\n" + toGraphString(v -> v.dynamicAnalysis.result) +
                "\nStatic Graphs:\n" + toGraphString(v -> v.staticAnalysis.result) +
                "\nCHA Graphs:\n" + toGraphString(v -> v.chaAnalysis.result);
    }

    private static String toGraphString(Function<VersionResult, Graph<GraphMethod, DefaultEdge>> toGet) {
        return versionResultList.stream()
                .filter(v -> toGet.apply(v) != null)
                .map(v -> toGet.apply(v).toString())
                .collect(Collectors.joining("\n"));
    }

    public static String toPositivityRate() {
        return toPositivityRate("Static Analysis", v -> v.staticAnalysis.result) + "\n" +
                toPositivityRate("CHA Analysis", v -> v.chaAnalysis.result);
    }

    private static String toPositivityRate(String name, Function<VersionResult, Graph<GraphMethod, DefaultEdge>> toGet) {
        return versionResultList.stream()
                .filter(v -> v.dynamicAnalysis.result != null && toGet.apply(v) != null)
                .map(v -> {
                    GraphAnalyser graphAnalyser = new GraphAnalyser(v.dynamicAnalysis.result);
                    int falsePositives = graphAnalyser.falsePositives(toGet.apply(v));
                    int truePositives = graphAnalyser.truePositives(toGet.apply(v));
                    int falseNegatives = graphAnalyser.falseNegative(toGet.apply(v));

                    return String.format("%s - %s:\nFP: %d\nTP: %d\nFN: %d\n",
                            name,
                            v.commitHash,
                            falsePositives,
                            truePositives,
                            falseNegatives);
                })
                .collect(Collectors.joining("\n"));
    }
}
