package nz.ac.vuw.ecs.hazlehjack.results;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.GraphUtils;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Used to extract useful statistics from graphs
 */
public class GraphAnalyser {
    private final Graph<GraphMethod, DefaultEdge> graph;

    /**
     * If input is null, an empty graph is used instead
     * @param graph
     */
    public GraphAnalyser(Graph<GraphMethod, DefaultEdge> graph) {
        if (graph == null) {
            graph = new DefaultDirectedGraph<> (DefaultEdge.class);
        }

        this.graph = graph;
    }

    public static GraphAnalyser of(Graph<GraphMethod, DefaultEdge> graph) {
        return new GraphAnalyser(graph);
    }

    public int getNumEdges() {
        return graph.edgeSet().size();
    }

    public int getNumVertices() {
        return graph.vertexSet().size();
    }

    public int truePositives(Graph<GraphMethod, DefaultEdge> toCompareWith) {
        return trueFalsePositiveNegativeHelper(toCompareWith, e -> graph.containsEdge(e));
    }

    public int falsePositives(Graph<GraphMethod, DefaultEdge> toCompareWith) {
        return trueFalsePositiveNegativeHelper(toCompareWith, e -> !graph.containsEdge(e));
    }

    public int falseNegative(Graph<GraphMethod, DefaultEdge> toCompareWith) {
        return trueFalsePositiveNegativeHelper(graph, e -> !toCompareWith.containsEdge(e));
    }

    private int trueFalsePositiveNegativeHelper(Graph<GraphMethod, DefaultEdge> graph, Predicate<DefaultEdge> predicate) {
        return (int) graph.edgeSet().stream()
                .filter(predicate)
                .count();
    }

    @Override
    public String toString() {
        return "GraphAnalyser{" +
                "numEdges=" + getNumEdges() +
                ",numVertices=" + getNumVertices() +
                ",testMethods=" + Arrays.toString(GraphUtils.getTestMethods(graph).stream().map(GraphMethod::getQualifiedMethodName).toArray()) +
                '}';
    }
}
