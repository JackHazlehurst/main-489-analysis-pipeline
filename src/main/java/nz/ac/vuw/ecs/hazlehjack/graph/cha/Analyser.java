package nz.ac.vuw.ecs.hazlehjack.graph.cha;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.ChaClass;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Method;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Pair;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.PhantomChaClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.ClassReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Analyser {
    private static final Logger LOGGER = LogManager.getLogger(Analyser.class);
    public Set<Pair<Method, Method>> recordedEdges = Collections.synchronizedSet(new HashSet<>());
    public Set<ChaClass> recordedClasses = Collections.synchronizedSet(new HashSet<>());

    /**
     *
     * @param args path to class files, dirs recursively searched
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            LOGGER.error("Need 1 argument");
            System.exit(1);
        }

        new Analyser().analyse(Paths.get(args[0]));
    }

    public Collection<Pair<Method, Method>> analyse(Path dir) {
        Set<Pair<Method, Method>> edges = Collections.synchronizedSet(new HashSet<>());

        try {
            Files.walk(dir)
                    .parallel()
                    .filter(f -> f.toString().endsWith(".class"))
                    .forEach(this::analyseHelper);

            //Swap classes from method pairs with classes found with hierarchy information
            recordedEdges = replaceClassesInMethodPairs(recordedClasses, recordedEdges);

            // Extract missing classes and find missing parents
            recordedClasses = extractMissingClassesFromEdges(recordedClasses, recordedEdges);
            recordedClasses = getStandardLibraryParents(recordedClasses);

            edges.addAll(this.recordedEdges);
            edges.addAll(getOtherEdges(recordedClasses, recordedEdges));
            this.recordedEdges = edges;

            // Identify phantom classes
            recordedClasses = transformPhantomClasses(this.recordedClasses);
        }
        catch (IOException e) {
            LOGGER.error("Error finding class files: {}", e);
            System.exit(1);
        }

        return edges;
    }

    private void analyseHelper(Path classFile) {
        try {
            ClassReader reader = new ClassReader(new FileInputStream(classFile.toFile()));
            ClassAnalyserVisitor visitor = new ClassAnalyserVisitor(this);
            reader.accept(visitor, ClassReader.EXPAND_FRAMES);
        }
        catch (IOException e) {
            LOGGER.error("Error reading from file: " + e.getStackTrace());
            System.exit(1);
        }
    }

    /**
     * Transforms all objects with no parents to phantom classes (java.lang.Object is exempt)
     * @param classes
     * @return
     */
    private static Set<ChaClass> transformPhantomClasses(Set<ChaClass> classes) {
        return classes.stream()
                .map(c -> !c.hasParent() && !c.isJavaLangObject() ? PhantomChaClass.of(c) : c)
                .collect(Collectors.toSet());
    }

    private static Set<Pair<Method, Method>> replaceClassesInMethodPairs(Collection<ChaClass> classes, Collection<Pair<Method, Method>> edges) {
        return edges.parallelStream()
                .map(p -> {
                    Method caller = p.getKey();
                    Method callee = p.getValue();
                    ChaClass callerClass = Util.getFromCollection(classes, caller.clazz);
                    ChaClass calleeClass = Util.getFromCollection(classes, callee.clazz);

                    if (callerClass == null) {
                        calleeClass = p.getKey().clazz;
                    }
                    if (calleeClass == null) {
                        calleeClass = p.getValue().clazz;
                    }

                    caller = new Method(callerClass, caller);
                    callee = new Method(calleeClass, callee);

                    return new Pair<>(caller, callee);
                })
                .collect(Collectors.toSet());
    }

    public static Set<ChaClass> extractMissingClassesFromEdges(Set<ChaClass> classes, Set<Pair<Method, Method>> edges) {
        Set<ChaClass> newClasses = Collections.synchronizedSet(new HashSet<>(classes));

        edges.stream()
                .flatMap(p -> Stream.of(p.getKey(), p.getValue()))
                .map(m -> m.clazz)
                .forEach(c -> newClasses.add(c));

        return newClasses;
    }

    public static Set<ChaClass> getStandardLibraryParents(Set<ChaClass> classSet) {
        Collection<List<ChaClass>> groupedClasses = classSet.stream()
                .flatMap(c -> findParents(c).stream())
                .collect(Collectors.groupingBy(Function.identity()))
                .values();

        return groupedClasses.stream()
                .map(cl -> {
                    if (cl.size() == 1) {
                        return cl.get(0);
                    }

                    return cl.stream()
                            .reduce((a, b) -> ChaClass.merge(a, b))
                            .orElseThrow(() -> new IllegalStateException("Internal error, could not reduce classes"));
                })
                .collect(Collectors.toSet());
    }

    private static Set<ChaClass> findParents(ChaClass chaChild) {
        return findParentsHelper(new HashSet<>(), chaChild);
    }

    private static Set<ChaClass> findParentsHelper(Set<ChaClass> classes, ChaClass chaChild) {
        classes.add(chaChild);

        if (chaChild.parent != null || chaChild.isJavaLangObject()) {
            return classes;
        }

        try {
            Class childClass = Class.forName(chaChild.getFullName());
            ChaClass chaParent;

            if (childClass.getSuperclass() != null) {
                Class parentClass = childClass.getSuperclass();
                String parentPackage = parentClass.getPackage().getName();
                chaParent = ChaClass.of(parentPackage, parentClass.getSimpleName());
            }
            else {
                chaParent = ChaClass.chaInterface;
            }

            chaParent.addChild(chaChild);
            chaChild.setParent(chaParent);

            if (classes.contains(chaParent)) {
                ChaClass existingParent = Util.getFromCollectionOrThrow(classes, chaParent);
                ChaClass mergedParent = ChaClass.merge(existingParent, chaParent);

                classes.remove(existingParent);
                classes.add(mergedParent);
            }

            return findParentsHelper(classes, chaParent);
        }
        catch (ClassNotFoundException e) {
            return classes;
        }
    }

    /**
     * Adds edges from the child classes to ensure all possible method calls are included
     * @param classes where to search for child classes
     * @param edges edges to look for children
     * @return new edges found
     */
    public static Collection<Pair<Method, Method>> getOtherEdges(Collection<ChaClass> classes, Collection<Pair<Method, Method>> edges) {
        Collection<Pair<Method, Method>> newEdges = Collections.synchronizedSet(new HashSet<>());

        edges.parallelStream()
                .forEach(e -> {
                    ChaClass callee = Util.getFromCollection(classes, e.getValue().clazz);

                    if (callee == null) {
                        LOGGER.warn("Could not find callee class in recognised classes: " + e.getValue().clazz);
                        return;
                    }

                    Set<ChaClass> children = callee.getIndirectChildren();
                    children.forEach(c -> newEdges.add(new Pair<>(e.getKey(), new Method(c, e.getValue()))));
                });

        return newEdges;
    }
}
