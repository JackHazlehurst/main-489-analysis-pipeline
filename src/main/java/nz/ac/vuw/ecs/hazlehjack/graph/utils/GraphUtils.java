package nz.ac.vuw.ecs.hazlehjack.graph.utils;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Pair;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

public class GraphUtils {
    private static final Logger LOGGER = LogManager.getLogger(GraphUtils.class);

    public static void testsToCsv(Graph<GraphMethod, DefaultEdge> graph, Path graphPath) {
        testsToCsv(graph, graphPath, getTestMethods(graph));
    }

    public static void testsToCsv(Graph<GraphMethod, DefaultEdge> graph, Path graphPath, Set<GraphMethod> testMethods) {
        testMethods.addAll(getTestMethods(graph));

        try {
            BufferedWriter writer = Files.newBufferedWriter(graphPath);
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.TDF
                    .withHeader("testClass", "testMethod", "class", "name", "descriptor"));

            testMethods.stream()
                    .flatMap(m -> getChildrenOfTest(graph, m).stream())
                    .forEach(p -> {
                        GraphMethod test = p.getKey();
                        GraphMethod method = p.getValue();
                        String testClass = test.getQualifiedClassName();
                        String testMethod = test.methodName;
                        String clazz = method.getQualifiedClassName();
                        String name = method.methodName;
                        String descriptor = method.toTrackedInvocationsDescriptor();

                        try {
                            csvPrinter.printRecord(testClass, testMethod, clazz, name, descriptor);
                        } catch (IOException e) {
                            LOGGER.error("Error making csv...\n{}", e.getStackTrace());
                            System.exit(1);
                        }
                    });
            csvPrinter.flush();
        }
        catch (IOException e) {
            LOGGER.error("Error making csv...\n{}", e.getStackTrace());
            System.exit(1);
        }
    }

    private static Set<Pair<GraphMethod, GraphMethod>> getChildrenOfTest(Graph<GraphMethod, DefaultEdge> graph, GraphMethod test) {
        Set<GraphMethod> testSet = new HashSet<>();
        testSet.add(test);

        return getAllChildren(graph, testSet).stream()
                .map(m -> new Pair<>(test, m))
                .collect(Collectors.toSet());
    }

    public static Set<GraphMethod> getTestMethods(Graph<GraphMethod, DefaultEdge> graph) {
        return graph.vertexSet().stream()
                .filter(GraphMethod::hasTestAnnotation)
                .collect(Collectors.toSet());
    }

    public static Set<GraphMethod> getAllChildren(Graph<GraphMethod, DefaultEdge> graph, GraphMethod childToGet) {
        Set<GraphMethod> toGetSet = new HashSet<>();
        toGetSet.add(childToGet);

        return getAllChildren(graph, toGetSet);
    }

    public static Set<GraphMethod> getAllChildren(Graph<GraphMethod, DefaultEdge> graph, Set<GraphMethod> childrenToGet) {
        return getAllChildrenHelper(graph, new HashSet<>(childrenToGet));
    }

    private static Set<GraphMethod> getAllChildrenHelper(Graph<GraphMethod, DefaultEdge> graph,
                                                         Set<GraphMethod> childrenToGet) {
        Set<GraphMethod> methodsFound = new HashSet<>();
        Stack<GraphMethod> workStack = new Stack<>();
        workStack.addAll(childrenToGet);

        while (!workStack.isEmpty()) {
            GraphMethod toFind = workStack.pop();
            if (methodsFound.contains(toFind)) {
                continue;
            }
            methodsFound.add(toFind);
            if (!graph.containsVertex(toFind)) {
                continue;
            }

            graph.outgoingEdgesOf(toFind).stream()
                    .map(graph::getEdgeTarget)
                    .forEach(workStack::add);
        }

        return methodsFound;
    }

    /**
     * Note: Will return empty set if toFind is a test
     * @param tests set of tests in graph
     * @param graph
     * @param toFind all tests that can reach this method will be returned
     * @return
     */
    public static Set<GraphMethod> selectTestParents(Set<GraphMethod> tests,
                                                     Graph<GraphMethod, DefaultEdge> graph,
                                                     GraphMethod toFind) {
        if(!graph.containsVertex(toFind) || tests.contains(toFind)) {
            return new HashSet<>();
        }

        Set<GraphMethod> selectedTests = new HashSet<>();
        Set<GraphMethod> haveVisited = new HashSet<>();
        Stack<GraphMethod> toVisit = new Stack<>();
        toVisit.add(toFind);

        while(!toVisit.isEmpty()) {
            if(haveVisited.contains(toVisit.peek())) {
                toVisit.pop();
                continue;
            }

            GraphMethod visiting = toVisit.pop();
            haveVisited.add(visiting);
            if (tests.contains(visiting)) {
                selectedTests.add(visiting);
                continue;
            }

            graph.incomingEdgesOf(visiting).stream()
                    .map(graph::getEdgeSource)
                    .forEach(toVisit::add);
        }

        return selectedTests;
    }

    public static Set<GraphMethod> selectTests(Graph<GraphMethod, DefaultEdge> v1,
                                                          Graph<GraphMethod, DefaultEdge> v2) {
        Set<GraphMethod> changedTests = new HashSet<>();
        Set<GraphMethod> allTests = getTestMethods(v1);
        allTests.addAll(getTestMethods(v2));

        for(GraphMethod test : allTests) {
            Set<GraphMethod> v1Test = new HashSet<>();
            Set<GraphMethod> v2Test = new HashSet<>();
            if (v1.containsVertex(test)) {
                v1Test = getAllChildren(v1, test);
            }
            if (v2.containsVertex(test)) {
                v2Test = getAllChildren(v2, test);
            }

            if (!v1Test.equals(v2Test)) {
                changedTests.add(test);
            }
        }

        return changedTests;
    }
}
