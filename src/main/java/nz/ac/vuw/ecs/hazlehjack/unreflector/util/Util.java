package nz.ac.vuw.ecs.hazlehjack.unreflector.util;

import org.apache.commons.lang.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

public class Util {
    private static final Logger LOGGER = LogManager.getLogger(Util.class);

    public static void createDirectory(Path dir) {
        File newDir = new File(dir.toString());

        if (newDir.exists()) {
            LOGGER.warn(dir + " already exists");
        }
        else {
            newDir.mkdir();
        }
    }

    public static Process runProcessAsRootOnLinux(String command, String errorMsg, Path dir) {
        if (SystemUtils.IS_OS_LINUX) {
            command = "sudo " + command;
        }
        return runProcess(command, errorMsg, dir);
    }

    public static Process runProcessAsRootOnLinux(String command, String errorMsg) {
        if (SystemUtils.IS_OS_LINUX) {
            command = "sudo " + command;
        }
        return runProcess(command, errorMsg);
    }

    public static Process runProcess(String command, String errorMsg) {
        return runProcessHelper(new ProcessBuilder(), command, errorMsg, Optional.empty());
    }

    public static Process runProcess(String command, String errorMsg, Path dir) {
        return runProcessHelper(new ProcessBuilder(), command, errorMsg, Optional.of(dir));
    }

    public static Process runProcess(ProcessBuilder builder, String command, String errorMsg, Path dir) {
        return runProcessHelper(builder, command, errorMsg, Optional.of(dir));
    }

    private static Process runProcessHelper(ProcessBuilder builder, String command, String errorMsg, Optional<Path> dir) {
        try {
            dir.ifPresent((d) -> builder.directory(d.toFile()));
            builder.command(command.split("\\s+"));

            Process process = builder.start();
            process.waitFor();

            return process;
        } catch (IOException | InterruptedException e) {
            LOGGER.error(errorMsg + " {}: ", e.getMessage());
            System.exit(1);
        }
        throw new IllegalStateException(errorMsg);
    }

    public static int extractJar(Path jar, Path extractionDir) {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("jar -xf " + jar, new String[]{}, extractionDir.toFile());

            process.waitFor();
            return process.exitValue();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException("Unexpected condition while extracting jar");
    }
}
