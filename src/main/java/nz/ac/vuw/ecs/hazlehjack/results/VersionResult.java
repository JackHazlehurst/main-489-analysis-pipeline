package nz.ac.vuw.ecs.hazlehjack.results;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;

import java.util.Optional;
import java.util.Set;

public class VersionResult {
    public final String commitHash;
    public final AnalysisResult dynamicAnalysis;
    public final AnalysisResult staticAnalysis;
    public final AnalysisResult chaAnalysis;
    public final Optional<Set<GraphMethod>> optionalTestSet;

    public VersionResult(String commitHash,
                         AnalysisResult dynamicAnalysis,
                         AnalysisResult staticAnalysis,
                         AnalysisResult chaAnalysis) {
        this.commitHash = commitHash;
        this.dynamicAnalysis = dynamicAnalysis;
        this.staticAnalysis = staticAnalysis;
        this.chaAnalysis = chaAnalysis;
        this.optionalTestSet = Optional.empty();
    }

    public VersionResult(String commitHash,
                         AnalysisResult dynamicAnalysis,
                         AnalysisResult staticAnalysis,
                         AnalysisResult chaAnalysis,
                         Optional<Set<GraphMethod>> testSet) {
        this.commitHash = commitHash;
        this.dynamicAnalysis = dynamicAnalysis;
        this.staticAnalysis = staticAnalysis;
        this.chaAnalysis = chaAnalysis;
        this.optionalTestSet = testSet;
    }

    public static VersionResult of(String commitHash) {
        return new VersionResult(commitHash, AnalysisResult.emptyPair, AnalysisResult.emptyPair, AnalysisResult.emptyPair);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VersionResult that = (VersionResult) o;

        return commitHash != null ? commitHash.equals(that.commitHash) : that.commitHash == null;
    }

    @Override
    public int hashCode() {
        return commitHash != null ? commitHash.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "VersionResult{" +
                "\ncommitHash=" + commitHash + '\'' +
                ",\n dynamicAnalysis=" + dynamicAnalysis.toSummaryString() +
                ",\n staticAnalysis=" + staticAnalysis.toSummaryString() +
                ",\n chaAnalysis=" + chaAnalysis.toSummaryString()  +
                "\n}\n";
    }
}
