package nz.ac.vuw.ecs.hazlehjack.dataStructures;

import nz.ac.vuw.ecs.hazlehjack.PomUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.Optional;

public abstract class PomElement {
    public final String groupId;
    public final String artifactId;
    public final String version;
    public final String parentNodeName;
    public final String nodeName;

    protected PomElement(String groupId, String artifactId, String version, String parentNodeName, String nodeName) {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = version;
        this.parentNodeName = parentNodeName;
        this.nodeName = nodeName;
    }

    public Node toNode(Document document) {
        Node parent = document.createElement(nodeName);
        PomUtil.appendChildNode(document, parent, "groupId", Optional.of(groupId));
        PomUtil.appendChildNode(document, parent, "artifactId", Optional.of(artifactId));
        PomUtil.appendChildNode(document, parent, "version", Optional.of(version));

        return parent;
    }

}
