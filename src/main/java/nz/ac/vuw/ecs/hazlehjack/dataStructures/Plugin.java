package nz.ac.vuw.ecs.hazlehjack.dataStructures;

import nz.ac.vuw.ecs.hazlehjack.PomUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.Optional;

public class Plugin extends PomElement{
    public final Optional<String> argLine;

    public Plugin(String groupId, String artifactId, String version, String argLine) {
        super(groupId, artifactId, version, "plugins", "plugin");
        this.argLine = Optional.of(argLine);
    }

    @Override
    public Node toNode(Document document) {
        Node parent = super.toNode(document);

        argLine.ifPresent(al -> {
            Node confNode = PomUtil.appendChildNode(document, parent, "configuration", Optional.empty());
            PomUtil.appendChildNode(document, confNode, "argLine", argLine);
        });

        return parent;
    }
}
