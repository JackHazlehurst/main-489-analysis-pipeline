package nz.ac.vuw.ecs.hazlehjack.unreflector;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JUnitModel {
    private String className;
    private List<String> testMethods=new ArrayList<>();
    private String before;
    private String beforeClass;
    private String after;
    private String afterClass;
    private String parameterizedMethod;
    private List<String> constructorParams=new LinkedList<>();


    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<String> getTestMethods() {
        return testMethods;
    }

    public void addTestMethod(String method) {
        this.testMethods.add(method);
    }

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getBeforeClass() {
        return beforeClass;
    }

    public void setBeforeClass(String beforeClass) {
        this.beforeClass = beforeClass;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    public String getAfterClass() {
        return afterClass;
    }

    public void setAfterClass(String afterClass) {
        this.afterClass = afterClass;
    }


    public String getParameterizedMethod() {
        return parameterizedMethod;
    }
    public void setParameterizedMethod(String parameterizedMethod) {
        this.parameterizedMethod =parameterizedMethod;
    }


    public void addConstructorParam(String param) {
        this.constructorParams.add(param);
    }


    public List<String> getConstructorParam() {
        return this.constructorParams;
    }
}
