package nz.ac.vuw.ecs.hazlehjack.graph;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.util.mxCellRenderer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultEdge;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class GraphMain {
    private static final Logger LOGGER = LogManager.getLogger(GraphMain.class);
    private static final String GRAPH_DIR = System.getProperty("user.dir") + "/src/main/resources/graph-images/";

    /**
     *
     * @param args Expects the path to the Call Graph csv as the only argument.
     */
    public static void main (String[] args) {
        if (args.length == 1) {
            Graph<GraphMethod, DefaultEdge> graph = CallGraphParser.parse(Paths.get(args[0]));
            CallGraphParser.parseCha(graph, Paths.get("/Users/jackh/src/doop/call-graph-edge/data"));




            graph.vertexSet().stream()
                    .filter(v -> v.className.equals("abcTest"))
                    .collect(Collectors.toSet());



//            performReachability(graph,);





            graph.edgeSet();
//            try {
//                visualiseGraph(graph, "graph-" + System.currentTimeMillis());
//            } catch (IOException e) {
//                LOGGER.error("Failed to visualise graph");
//            }
        }
        else {
            LOGGER.error("Incorrect arguments, expected path to csv file", args);
            System.exit(1);
        }
    }

    public static <V, E> void visualiseGraph(Graph<V, E> g, String imageName) throws IOException {
        JGraphXAdapter<V, E> graphAdapter = new JGraphXAdapter<>(g);
        mxIGraphLayout layout = new mxCircleLayout(graphAdapter);
        layout.execute(graphAdapter.getDefaultParent());

        BufferedImage image = mxCellRenderer.createBufferedImage(graphAdapter, null, 2, Color.WHITE, true, null);
        File imgFile = new File(GRAPH_DIR + imageName + ".png");
        ImageIO.write(image, "PNG", imgFile);
    }

    public static Collection<java.lang.Class> performReachability(Graph<java.lang.Class, DefaultEdge> graph, java.lang.Class clazz) {
        Set<java.lang.Class> classes = new HashSet<>();
        graph.outgoingEdgesOf(clazz).forEach(e -> performReachabilityHelper(classes, graph, e));
        return classes;
    }

    private static void performReachabilityHelper(Set<java.lang.Class> classes, Graph<java.lang.Class, DefaultEdge> graph, DefaultEdge edge) {
        java.lang.Class targetClass = graph.getEdgeTarget(edge);
        classes.add(targetClass);
        graph.outgoingEdgesOf(targetClass).forEach(tc -> performReachabilityHelper(classes, graph, tc));
    }
}
