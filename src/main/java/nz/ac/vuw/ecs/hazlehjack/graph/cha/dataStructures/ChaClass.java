package nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.Util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ChaClass {
    public static final ChaClass chaInterface = new ChaClass("Interfaces");

    /**
     * Used to differentiate special cases.
     */
    public final boolean isSpecial;
    public final String packageName;
    public final String className;
    public ChaClass parent;
    public Set<ChaClass> children = Collections.synchronizedSet(new HashSet<>());

    public ChaClass(String packageName, String className) {
        this.isSpecial = false;
        this.packageName = packageName;
        this.className = className;
    }

    private ChaClass(String className) {
        this.isSpecial = true;
        this.packageName = "";
        this.className = className;
    }

    public static ChaClass of(String packageName, String className) {
        return new ChaClass(packageName, className);
    }

    public static ChaClass of(String className) {
        return new ChaClass("", className);
    }

    /**
     * Parents must be the same, unless one is null and the other is not null.
     * @param a
     * @param b
     * @return
     */
    public static ChaClass merge(ChaClass a, ChaClass b) {
        if (a == null && b == null) {
            throw new IllegalArgumentException("Both arguments cannot be null");
        }

        if (!a.packageName.equals(b.packageName) || !a.className.equals(b.className)) {
            throw new IllegalArgumentException("Classes cannot be merged; package or class name do not match " + a + " " + b);
        }

        ChaClass parent;
        if (a.hasParent() && b.hasParent() && !a.parent.equals(b.parent)) {
            throw new IllegalArgumentException("Classes cannot be merged; parents do not match " + a + " " + b);
        }
        else if (a.hasParent()) {
            parent = a.parent;
        }
        else {
            parent = b.parent;
        }

        ChaClass newClass = of(a.packageName, a.className);
        newClass.setParent(parent);
        copyClassRelationships(newClass, a);
        copyClassRelationships(newClass, b);

        return newClass;
    }

    private static void copyClassRelationships(ChaClass newClass, ChaClass oldClass) {
        oldClass.getDirectChildren()
                .forEach(c -> {
                    c.setParent(newClass);

                    if (newClass.getDirectChildren().contains(c)) {
                        ChaClass newChild = newClass.getDirectChild(c);
                        ChaClass oldChild = c;
                        ChaClass mergedChild = merge(newChild, oldChild);

                        newClass.children.remove(newChild);
                        newClass.children.add(mergedChild);
                    }
                    else {
                        newClass.addChild(c);
                    }
                });
    }

    public ChaClass getParent() {
        return parent;
    }

    public void setParent(ChaClass parent) {
        this.parent = parent;
    }

    public boolean hasParent() {
        return this.parent != null;
    }

    public ChaClass getDirectChild(ChaClass childToGet) {
        return Util.getFromCollectionOrThrow(this.children, childToGet);
    }

    public Set<ChaClass> getDirectChildren() {
        return Collections.synchronizedSet(new HashSet<>(children));
    }

    public Set<ChaClass> getIndirectChildren() {
        Set<ChaClass> allChildren = new HashSet<>(children);
        children.forEach(c -> allChildren.addAll(c.getIndirectChildren()));

        return allChildren;
    }

    public void addChild(ChaClass child) {
        this.children.add(child);
    }

    public String getFullName() {
        return packageName.isEmpty() ? className : packageName + "." + className;
    }

    public boolean isPhantomClass() {
        return false;
    }

    public boolean isJavaLangObject() {
        return packageName.equals("java.lang") && className.equals("Object");
    }

    @Override
    public String toString() {
        String parentClass = parent == null ? "null" : parent.getFullName();

        return "ChaClass{" +
                "packageName='" + packageName + '\'' +
                ", className='" + className + '\'' +
                ", parent=" + parentClass +
                ", children=" + children.stream()
                    .map(ChaClass::getFullName)
                    .collect(Collectors.joining(", ", "[", "]")) +
                '}';
    }

    /**
     * Only takes into account package and class name
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChaClass chaClass = (ChaClass) o;

        if (packageName != null ? !packageName.equals(chaClass.packageName) : chaClass.packageName != null)
            return false;
        return className != null ? className.equals(chaClass.className) : chaClass.className == null;
    }

    /**
     * Only takes into account package and class name
     * @return
     */
    @Override
    public int hashCode() {
        int result = packageName != null ? packageName.hashCode() : 0;
        result = 31 * result + (className != null ? className.hashCode() : 0);
        return result;
    }
}
