package nz.ac.vuw.ecs.hazlehjack.unreflector.doop;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.PullImageResultCallback;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.core.DockerClientBuilder;
import nz.ac.vuw.ecs.hazlehjack.unreflector.util.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.ProcessingException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DoopDocker {
    private static final Logger LOGGER = LogManager.getLogger(DoopDocker.class);
    private final Path SHARED_DIR;
    private final Path SCRIPT;
    private final Path JAR;
    private final String OPTIONS;
    private final String DOOP_IMAGE = "gfour/doop:4.21.6";
    private final String DOOP_IMAGE_NO_TAG = "gfour/doop";
    private final String TAG = "4.21.6";
    private final String DATA_DIR = "/data";
    private final String DOOP_HOME = "$DOOP_HOME";

    public DoopDocker(Path sharedDir, Path jar, String options) {
        this.SHARED_DIR = sharedDir;
        this.SCRIPT = Paths.get(SHARED_DIR.toString(), "doop_container.sh");
        this.JAR = jar;
        this.OPTIONS = options;
    }

    public void analyse() {
        try {
            analyseDoop();
        } catch (ProcessingException e) {
            LOGGER.error("Is the docker daemon running? {}", e.toString());
            System.exit(1);
        } catch (InterruptedException e) {
            LOGGER.error("Docker was interrupted while pulling an image: {}", e.toString());
            System.exit(1);
        }
    }

    private void analyseDoop() throws InterruptedException {
        DockerClient client = DockerClientBuilder.getInstance().build();

        client.pullImageCmd(DOOP_IMAGE_NO_TAG)
                .withTag(TAG)
                .exec(new PullImageResultCallback())
                .awaitCompletion();

        CreateContainerResponse container = client.createContainerCmd(DOOP_IMAGE)
                .withBinds(Bind.parse(this.SHARED_DIR + ":" + this.DATA_DIR))
                .withTty(true)
                .exec();

        try {
            client.startContainerCmd(container.getId()).exec();
            LOGGER.info("DOOP Container ID: " + container.getId());
            createScript();
            runScript(container.getId());
        }
        catch (Exception e) {
            LOGGER.error("Docker error: {}", e.toString());
        }
        finally {
            LOGGER.info("Stopping container: {}", container.getId());
            client.stopContainerCmd(container.getId()).exec();
        }
    }

    private void createScript() {
        String script = "#!/bin/bash\n" +
                String.format("%s/bin/doop -i %s %s\n", DOOP_HOME, Paths.get(DATA_DIR, JAR.toString()), OPTIONS) +
                "cp -Lr " + DOOP_HOME + "/last-analysis " + DATA_DIR + "\n";

        try (BufferedWriter out = new BufferedWriter(new FileWriter(SCRIPT.toString()))) {
            LOGGER.info("Creating script for container");
            out.write(script);
            SCRIPT.toFile().setExecutable(true);
        }
        catch (IOException e) {
            LOGGER.error("Error creating script for container: " + e);
            System.exit(1);
        }
    }

    private void runScript(String containerId) {
        String command = String.format("docker exec %s %s/%s", containerId, DATA_DIR, SCRIPT.getFileName());

        if (Util.runProcessAsRootOnLinux(command, "Failure running docker script").exitValue() != 0) {
            LOGGER.error("Docker script failure");
        }
    }
}
