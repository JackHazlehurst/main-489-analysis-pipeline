package nz.ac.vuw.ecs.hazlehjack.unreflector;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.unreflector.doop.DoopDocker;
import nz.ac.vuw.ecs.hazlehjack.unreflector.util.Jar;
import nz.ac.vuw.ecs.hazlehjack.unreflector.util.Util;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class StaticAnalysisMain {
    private static final Logger LOGGER = LogManager.getLogger(StaticAnalysisMain.class);
    private static final String JAVA_VERSION = "1.8";
    public static final String CALL_GRAPH_EDGE_CSV = "last-analysis/CallGraphEdge.csv";
    public final Path REPO, ROOT, UNREFLECTOR_INPUT, UNREFLECTOR_OUTPUT, JAR_OUTPUT, SHARED;
    /**
     * Tests identified during the unreflection process
     */
    public Set<GraphMethod> testSet = new HashSet<>();

    /**
     *
     * @param args Single file path to directory with jar to be analysed
     */
    public static StaticAnalysisMain mainDoopAnalysis(String[] args) {
        if (args == null || args.length != 1) {
            LOGGER.error("Invalid arguments: " + args);
            System.exit(1);
        }

        StaticAnalysisMain main = new StaticAnalysisMain(args[0]);
        LOGGER.info("Supplied dir: " + main.ROOT);
        unreflectTests(main);

        DoopDocker dd = new DoopDocker(
                main.SHARED,
                main.JAR_OUTPUT.getName(main.JAR_OUTPUT.getNameCount() - 1),
                "-a context-insensitive");
        dd.analyse();

        return main;
    }

    public static StaticAnalysisMain compileClasses(String dir) {
        StaticAnalysisMain main = new StaticAnalysisMain(dir);
        Util.createDirectory(main.ROOT);
        Util.createDirectory(main.UNREFLECTOR_INPUT);
        compileClasses(main.REPO, main.UNREFLECTOR_INPUT);

        return main;
    }

    private static void unreflectTests(StaticAnalysisMain main) {
        Util.createDirectory(main.ROOT);
        Util.createDirectory(main.UNREFLECTOR_INPUT);
        Util.createDirectory(main.UNREFLECTOR_OUTPUT);
        Util.createDirectory(main.SHARED);

        // These class files are needed to generate the unreflected tests
        compileClasses(main.REPO, main.UNREFLECTOR_INPUT);
        LOGGER.info("Unreflecting tests to: " + main.UNREFLECTOR_OUTPUT);
        try {
            main.testSet = BuiltinTestDriverGenerator.main(new String[]{main.UNREFLECTOR_INPUT.toString(), main.UNREFLECTOR_OUTPUT.toString()});
        } catch (IOException e) {
            LOGGER.error("Could not un-reflect tests :(");
            System.exit(1);
        }
        // The unreflected tests then need to be compiled for DOOP (along with dependencies)
        compileDependencies(main.REPO, main.UNREFLECTOR_INPUT);
        compileGeneratedClasses(main.UNREFLECTOR_OUTPUT);

        LOGGER.info("Creating jar for DOOP: " + main.JAR_OUTPUT);
        Jar.create(main.UNREFLECTOR_OUTPUT, main.JAR_OUTPUT, BuiltinTestDriverGenerator.ENTRY_POINT, main.JAVA_VERSION);
    }

    public StaticAnalysisMain(String dir) {
        this.REPO = Paths.get(dir);
        this.ROOT = Paths.get(dir + "-data");
        this.UNREFLECTOR_INPUT = Paths.get(ROOT.toString(), "input");
        this.UNREFLECTOR_OUTPUT = Paths.get(ROOT.toString(), "output");
        this.SHARED = Paths.get(ROOT.toString(), "shared");
        this.JAR_OUTPUT = Paths.get(SHARED.toString(), "unreflected.jar");
    }

    private static int compileClasses(Path toCompile, Path toSave) {
        LOGGER.info("Compiling: {}", toCompile);
        Process process = Util.runProcessAsRootOnLinux("mvn test-compile", "Error compiling project", toCompile);

        try {
            FileUtils.copyDirectory(Paths.get(toCompile.toString(), "target/classes").toFile(), toSave.toFile());
            FileUtils.copyDirectory(Paths.get(toCompile.toString(), "target/test-classes").toFile(), toSave.toFile());
        }
        catch (IOException e) {
            LOGGER.error("Error copying maven class files");
            System.exit(1);
        }

        return process.exitValue();
    }

    private static int compileDependencies(Path toCompile, Path toSave) {
        LOGGER.info("Compiling Dependencies: {}", toCompile);
        Process process = Util.runProcessAsRootOnLinux("mvn dependency:copy-dependencies", "Error copying dependencies ", toCompile);

        try {
            Files.walk(Paths.get(toCompile.toString(), "target/dependency"))
                    .forEach(p -> Util.extractJar(p, toSave));
        }
        catch (IOException e) {
            LOGGER.error("Error copying maven class files");
            System.exit(1);
        }

        return process.exitValue();
    }

    private static int compileGeneratedClasses(Path toCompile) {
            Process processCompile = Util.runProcessAsRootOnLinux("javac -source " + JAVA_VERSION + " -target " + JAVA_VERSION + " " + BuiltinTestDriverGenerator.ENTRY_POINT_JAVA,
                    "Error compiling generated classes",
                    toCompile);
            return processCompile.exitValue();
    }
}
