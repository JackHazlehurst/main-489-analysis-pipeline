package nz.ac.vuw.ecs.hazlehjack.results.rts;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.GraphUtils;
import nz.ac.vuw.ecs.hazlehjack.results.VersionResult;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;
import java.util.function.Function;

public class RTS {
    public static List<SelectedTests> selectTests(List<VersionResult> versionResultList) {
        if (versionResultList.size() < 2) {
            throw new IllegalArgumentException("Must have at least two versions to compare");
        }
        List<SelectedTests> selectedTestsList = new ArrayList<>();

        for(int i = 0; i < versionResultList.size() - 1; i++) {
            VersionResult v1 = versionResultList.get(i);
            VersionResult v2 = versionResultList.get(i + 1);
//            Set<GraphMethod> dynamicTests = selectTestsHelper(v1, v2, vr -> vr.dynamicAnalysis.result);
//            Set<GraphMethod> staticTests = selectTestsHelper(v1, v2, vr -> vr.staticAnalysis.result);
//            Set<GraphMethod> chaTests = selectTestsHelper(v1, v2, vr -> vr.chaAnalysis.result);

            Graph<GraphMethod, DefaultEdge> graphV1 = v1.chaAnalysis.result;
            Graph<GraphMethod, DefaultEdge> graphV2 = v2.chaAnalysis.result;
            // Needed as cha analysis only available from second result on
            if(i == 0) {
                graphV1 = v1.staticAnalysis.result;
            }

            Set<GraphMethod> selectedTests = GraphUtils.selectTests(graphV1, graphV2);
            selectedTestsList.add(new SelectedTests(new HashSet<>(), new HashSet<>(), selectedTests));
        }

        return selectedTestsList;
    }

    private static Set<GraphMethod> selectTestsHelper(VersionResult v1,
                                                      VersionResult v2,
                                                      Function<VersionResult, Graph<GraphMethod, DefaultEdge>> mapToGraph) {
        Graph<GraphMethod, DefaultEdge> graphV1 = mapToGraph.apply(v1);
        Graph<GraphMethod, DefaultEdge> graphV2 = mapToGraph.apply(v2);

        return GraphUtils.selectTests(graphV1, graphV2);
    }

    public static final class SelectedTests {
        private final Set<GraphMethod> dynamicMethods;
        private final Set<GraphMethod> staticMethods;
        private final Set<GraphMethod> chaMethods;

        public SelectedTests(Set<GraphMethod> dynamicMethods, Set<GraphMethod> staticMethods, Set<GraphMethod> chaMethods) {
            this.dynamicMethods = Collections.unmodifiableSet(dynamicMethods);
            this.staticMethods = Collections.unmodifiableSet(staticMethods);
            this.chaMethods = Collections.unmodifiableSet(chaMethods);
        }

        public Set<GraphMethod> getDynamicMethods() {
            return dynamicMethods;
        }

        public Set<GraphMethod> getStaticMethods() {
            return staticMethods;
        }

        public Set<GraphMethod> getChaMethods() {
            return chaMethods;
        }

        public String toCsv() {
            final String SEP = "\t";
            String csv = "dynamic" + SEP + "static" + SEP + "cha\n";
            Iterator<GraphMethod> dynamicIterator = dynamicMethods.iterator();
            Iterator<GraphMethod> staticIterator = staticMethods.iterator();
            Iterator<GraphMethod> chaIterator = chaMethods.iterator();


            while(dynamicIterator.hasNext() || staticIterator.hasNext() || chaIterator.hasNext()) {
                String dynamicItem = "";
                String staticItem = "";
                String chaItem = "";

                if (dynamicIterator.hasNext()) {
                    dynamicItem = dynamicIterator.next().toString();
                }
                if (staticIterator.hasNext()) {
                    staticItem = staticIterator.next().toString();
                }
                if (chaIterator.hasNext()) {
                    chaItem = chaIterator.next().toString();
                }

                csv += dynamicItem + SEP + staticItem + SEP + chaItem + "\n";
            }

            return csv;
        }
    }
}
