package nz.ac.vuw.ecs.hazlehjack.graph.cha;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.ChaClass;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Method;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static nz.ac.vuw.ecs.hazlehjack.graph.utils.Regex.BC_CLASS_ARGUMENT;


public class ClassAnalyserVisitor extends ClassVisitor {
    private static final Logger LOGGER = LogManager.getLogger(ClassAnalyserVisitor.class);
    private static final int VERSION = Opcodes.ASM8;
    private ChaClass caller;
    private Set<String> annotationSet = new HashSet<>();
    private Analyser analyser;

    public ClassAnalyserVisitor(Analyser analyser) {
        super(VERSION);
        this.analyser = analyser;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        annotationSet.clear();
        Method caller = new Method(this.caller, name, descriptor);

        return new MethodVisitor(VERSION) {
            @Override
            public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
                super.visitAnnotation(descriptor, visible);

                if (!descriptor.startsWith("L")) {
                    throw new IllegalStateException("All annotations should be classes");
                }

                Util.extractRegex(descriptor, BC_CLASS_ARGUMENT)
                        .ifPresent(a -> annotationSet.add(a.replace('/', '.')));
                return null;
            }

            @Override
            public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
                super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);


                ChaClass clazz = nameToClass(owner);
                Method callee = new Method(clazz, name, descriptor);

                analyser.recordedEdges.add(new Pair<>(caller.addAnnotations(annotationSet), callee));
            }
        };
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        if (signature == null && superName == null && name.equals("module-info")) {
            // This is ignoring the module files introduced in Java 9+ (not supported by this version of ASM)
            LOGGER.warn("Found 'module-info' class; ignoring");
            return;
        }

        Set<ChaClass> classes = analyser.recordedClasses;
        ChaClass child = nameToClass(name);
        ChaClass parent = nameToClass(superName);

        child = getExistingClass(classes, child);
        parent = getExistingClass(classes, parent);

        if (child.hasParent() && !child.parent.equals(parent)) {
            throw new InternalError("Child has unexpected parent");
        }

        child.setParent(parent);
        parent.addChild(child);

        classes.add(child);
        classes.add(parent);

        this.caller = child;
    }

    private static ChaClass nameToClass(String name) {
        String[] splitName = name.split("/");
        String pkg, clazz;

        if (splitName.length == 1) {
            pkg = "";
            clazz = splitName[0];
        }
        else {
            pkg = Arrays.stream(splitName)
                    .limit(splitName.length - 1)
                    .collect(Collectors.joining("."));
            clazz = splitName[splitName.length - 1];
        }

        return new ChaClass(pkg, clazz);
    }

    /**
     * Gets class from set if exists, else returns input
     * @param set
     * @param toGet
     * @return
     */
    public static ChaClass getExistingClass(Set<ChaClass> set, ChaClass toGet) {
        if (set.contains(toGet)) {
            ChaClass clazz = Util.getFromCollection(set, toGet);

            if (clazz == null) {
                throw new InternalError("Could not find class as expected: " + toGet);
            }
            return clazz;
        }
        return toGet;
    }
}