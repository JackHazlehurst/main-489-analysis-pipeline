package nz.ac.vuw.ecs.hazlehjack;

import nz.ac.vuw.ecs.hazlehjack.dataStructures.Dependency;
import nz.ac.vuw.ecs.hazlehjack.dataStructures.Plugin;
import nz.ac.vuw.ecs.hazlehjack.dataStructures.PomElement;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PomUtil {
    private static final Logger LOGGER = LogManager.getLogger(PomUtil.class);

    public static void addElementsToPom(Path pom, PomElement... elementArray) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(pom.toFile());

            List<PomElement> dependencyList = Arrays.stream(elementArray)
                    .filter(d -> d instanceof Dependency)
                    .collect(Collectors.toCollection(ArrayList::new));
            List<PomElement> pluginList = Arrays.stream(elementArray)
                    .filter(p -> p instanceof Plugin)
                    .collect(Collectors.toCollection(ArrayList::new));

            if (!dependencyList.isEmpty()) {
                addElementsToPomHelper(document, pom, dependencyList.toArray(new Dependency[]{}));
            }
            if (!pluginList.isEmpty()) {
                addElementsToPomHelper(document, pom, pluginList.toArray(new Plugin[]{}));
            }

            DOMSource source = new DOMSource(document);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult(pom.toString());
            transformer.transform(source, result);
        } catch (SAXException | IOException | ParserConfigurationException | TransformerException e) {
            LOGGER.error("Error inserting node in pom: {}", e);
            System.exit(1);
        }
    }

    public static void addPropertiesToPom(Path pom, Pair<String, String>... properties) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(pom.toFile());

            NodeList nodeList = document.getElementsByTagName("properties");

            if (nodeList.getLength() == 0) {
                appendChildNode(document, document.getFirstChild(), "properties", Optional.empty());
                nodeList = document.getElementsByTagName("properties");
            }

            Node propertiesNode = nodeList.item(0);
            propertiesNode.appendChild(document.createComment("The properties were dynamically inserted"));
            Arrays.stream(properties)
                    .forEach(p -> appendChildNode(
                            document,
                            propertiesNode,
                            p.getKey(),
                            Optional.of(p.getValue())));

            DOMSource source = new DOMSource(document);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult(pom.toString());
            transformer.transform(source, result);
        } catch (SAXException | IOException | ParserConfigurationException | TransformerException e) {
            LOGGER.error("Error inserting node in pom: {}", e);
            System.exit(1);
        }
    }

    private static <E extends PomElement> void addElementsToPomHelper(Document document, Path pom, E[] elementArray) {
        if (elementArray.length == 0) {
            return;
        }

        final boolean isPlugin = elementArray instanceof Plugin[];
        final String parentNodeName = elementArray[0].parentNodeName;
        NodeList nodeList = document.getElementsByTagName(parentNodeName);

        if (nodeList.getLength() == 0) {
            Node parent = isPlugin ?
                    appendChildNode(document, document.getFirstChild(), "build", Optional.empty()) :
                    document.getFirstChild();
            appendChildNode(document, parent, parentNodeName, Optional.empty());
            nodeList = document.getElementsByTagName(parentNodeName);
        }

        Node node;
        if (isPlugin) {
            node = selectCorrectPluginsNode(nodeList);
        }
        else {
            node = selectCorrectDependenciesNode(nodeList);
        }

        node.appendChild(document.createComment("Dynamic insertion starts here"));
        Arrays.stream(elementArray)
                .forEach(e -> node.appendChild(e.toNode(document)));
        node.appendChild(document.createComment("Dynamic insertion ends here"));
    }

    private static Node selectCorrectPluginsNode(NodeList nodes) {
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Node parent = node.getParentNode();

            if ("build".equals(parent.getNodeName()) && "project".equals(parent.getParentNode().getNodeName())) {
                return node;
            }
        }
        throw new IllegalArgumentException("Could not find correct node in list");
    }

    private static Node selectCorrectDependenciesNode(NodeList nodes) {
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Node parent = node.getParentNode();

            if ("project".equals(parent.getNodeName())) {
                return node;
            }
        }
        throw new IllegalArgumentException("Could not find correct node in list");
    }

    public static Node appendChildNode(Document document, Node patentNode, String nodeName, Optional<String> nodeContents) {
        Element newNode = document.createElement(nodeName);
        nodeContents.ifPresent(nc -> newNode.appendChild(document.createTextNode(nc)));
        patentNode.appendChild(newNode);

        return newNode;
    }
}
