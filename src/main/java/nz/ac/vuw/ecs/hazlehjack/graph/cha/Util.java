package nz.ac.vuw.ecs.hazlehjack.graph.cha;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Util {
    public static final String CLASS_DESCRIPTOR = "L";
    public static final String ARRAY_DESCRIPTOR = "[";
    private static final Map<String, String> descriptors = Stream.of(new String[][] {
            { "V", "void" },
            { "Z", "boolean" },
            { "C", "char" },
            { "B", "byte" },
            { "S", "short" },
            { "I", "int" },
            { "F", "float" },
            { "J", "long" },
            { "D", "double" },
            { CLASS_DESCRIPTOR, "<class>" },
    }).collect(Collectors.toMap(k -> k[0], v -> v[1]));

    public static boolean isValidType(String type) {
        if (type.startsWith(ARRAY_DESCRIPTOR)) {
            boolean allBrackets = type.substring(0, type.length() - 1)
                    .chars()
                    .allMatch(c -> c == '[');

            return allBrackets && descriptors.containsKey(type.substring(type.length() - 1));
        }
        else {
            return descriptors.containsKey(type);
        }
    }

    public static boolean isClass(String type) {
        String noArrayDescriptors = type.chars()
                .filter(c -> c != '[')
                .mapToObj(c -> String.valueOf((char)c))
                .collect(Collectors.joining());

        if (noArrayDescriptors.startsWith(ARRAY_DESCRIPTOR) && noArrayDescriptors.endsWith(";")) {
            return true;
        }
        return false;
    }

    public static boolean isClassArray(String type) {
        return isClass(type) && type.startsWith(ARRAY_DESCRIPTOR);
    }

    /**
     *
     * @return number of '[' at the start of the string
     */
    public static int getArrayNum(String type) {
        for (int i = 0; i < type.length(); i++) {
            if (type.charAt(i) != '[') {
                return i;
            }
        }

        return 0;
    }

    public static String parseType(String type) {
        if (canMatchRegex(type, "\\[*L([\\w\\/$]+);.*")) {
            return parseTypeClass(type);
        }

        int arrayNum = getArrayNum(type);
        type = type.substring(arrayNum);

        String longType = descriptors
                .computeIfAbsent(type, (t) -> parseFailure(t));
        return makeArray(longType, arrayNum);
    }

    private static String parseTypeClass(String clazzType) {
        String parsedType = Util.extractRegex(clazzType, "\\[*L([\\w\\/$]+);.*")
                .orElseThrow(() -> new IllegalArgumentException("Could not parse type: " + clazzType));

        parsedType = parsedType.replace('/', '.');
        parsedType = makeArray(parsedType, getArrayNum(clazzType));

        return parsedType;
    }

    private static String parseFailure(String type) {
        throw new IllegalArgumentException("Could not parse type: " + type);
    }

    public static String makeArray(String type, int times) {
        for (int i = 0; i < times; i++) {
            type += "[]";
        }
        return type;
    }

    public static boolean isArray(String type) {
        return type.startsWith(ARRAY_DESCRIPTOR);
    }

    public static Optional<String> extractRegex(String str, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        if (matcher.find()) {
            return Optional.of(matcher.group(1));
        }
        else {
            return Optional.empty();
        }
    }

    public static boolean canMatchRegex(String str, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        return matcher.find();
    }

    public static <E> E getFromCollection(Collection<E> collection, E element) {
        return new ArrayList<>(collection).parallelStream()
                .filter(e -> e.equals(element))
                .findAny().orElse(null);
    }

    public static <E> E getFromCollectionOrThrow(Collection<E> collection, E element) {
        E elemFound  = getFromCollection(collection, element);

        if (elemFound == null) {
            throw new NoSuchElementException("Element could not be found in collection: " + element);
        }
        return elemFound;
    }

    public static <E> E matchInCollectionOrReturn(Collection<E> collection, Predicate<E> toMatch, Supplier<E> orReturn) {
        return collection.stream()
                .filter(toMatch)
                .findAny()
                .orElse(orReturn.get());
    }
}
