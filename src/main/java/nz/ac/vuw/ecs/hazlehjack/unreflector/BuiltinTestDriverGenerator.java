package nz.ac.vuw.ecs.hazlehjack.unreflector;

import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.MethodInfo;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.Util;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class BuiltinTestDriverGenerator {
    private static final Logger LOGGER = LogManager.getLogger(BuiltinTestDriverGenerator.class);
    public static final String ENTRY_POINT = "EntryPoint_builtinTest";
    public static final String ENTRY_POINT_JAVA = ENTRY_POINT + ".java";
    public static Set<GraphMethod> testMethodSet = new HashSet<>();
    private static String outputDir;

    public static Set<GraphMethod> main(String[] args) throws IOException {
        if (args.length == 0) {
            throw new IllegalArgumentException("Invalid arguments: args[0]=inputDir args[1]=outputDir");
        }
        String inputDir = args[0];
        outputDir = args[1];
        testMethodSet.clear();

        Iterator it = FileUtils.iterateFiles(new File(inputDir), null, true);
        List<CtClass> loadedclasses = new ArrayList<>();
        ClassPool pool = new ClassPool();
        //load classes into classpool,
        while (it.hasNext()) {
            File f = (File) it.next();
            if (f.getName().endsWith(".class")) {
                try {
                    loadedclasses.add(pool.makeClass(new ByteArrayInputStream(IOUtils.toByteArray(new FileInputStream(f)))));
                }catch(IOException e){

                }
            }
        }
        int countDriver=0;
        //analysis all tests
        List<String> driverClazz=new ArrayList<>();
        for(CtClass clazz :loadedclasses) {
            //excluded tests in inner classes from analysis
            if(!clazz.getName().contains("$") && !clazz.isAnnotation() && !clazz.isEnum() && !clazz.isInterface() && !Modifier.isAbstract(clazz.getModifiers())) {

                JUnitModel model = new JUnitModel();

                try {
                    if(clazz.subclassOf(pool.get("junit.framework.TestCase"))){
                        anaylseJUnit3Method(clazz,model);
                    }
                    CtClass superClazz = clazz.getSuperclass();
                    if (superClazz != null) {
                        anaylseMethodAnnotation(superClazz, model);
                    }
                }catch (NotFoundException e){
                    System.err.println("----------------------------");
                    System.err.println("Can not find super class "+e.getMessage() +" of "+clazz.getName() );
                    System.err.println("----------------------------");
                }

                anaylseMethodAnnotation(clazz, model);
                try {
                    String driverName="Driver_builtinTest" + countDriver;
                    StringBuilder output=makeDriver(clazz.getName(), driverName, model);
                    if(output!=null) {
                        writeDriverToFile(output, driverName);
                        driverClazz.add(driverName);
                        countDriver++;
                    }

                }catch (IOException e){

                }
            }
        }

        StringBuilder entryPointSource=new StringBuilder();
        entryPointSource.append("public class " + ENTRY_POINT + " {\n");
        entryPointSource.append("public static void main(String[] args) throws Throwable{\n");
        for (String driver : driverClazz) {
            entryPointSource.append(driver+".main(null);\n");
        }
        entryPointSource.append("}\n");
        entryPointSource.append("}");
        System.out.println("generated "+countDriver+" builtin drivers");
        FileUtils.writeStringToFile(new File(outputDir+"/" + ENTRY_POINT_JAVA),entryPointSource.toString(), Charset.defaultCharset());

        // remove JUnit references
        for(CtClass clazz :loadedclasses) {
            try {
                addConstructor(clazz);
                //removeAnnotations(clazz);
                clazz.writeFile(outputDir);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        return testMethodSet;
    }

    private static void addConstructor(CtClass clazz){
        if(clazz.getName().contains("$") || clazz.isAnnotation() || clazz.isEnum() || clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers())
                || Modifier.isProtected(clazz.getModifiers())||Modifier.isPrivate(clazz.getModifiers())) {
            return;
        }

        CtConstructor[] constructors=clazz.getDeclaredConstructors();
        for(CtConstructor constructor: constructors){
            if(constructor.getSignature().equals("()V")){
                return;
            }
        }
        try {
            CtConstructor con = CtNewConstructor.make("public "+clazz.getSimpleName()+"(){}", clazz);
            clazz.addConstructor(con);
        }catch (CannotCompileException e){
            System.err.println("----------------------------");
            System.err.println("Can not add constructor to "+clazz.getName() );
            System.err.println("----------------------------");
        }
    }

    public static StringBuilder makeDriver(String targetClassName, String driverClassName, JUnitModel model) throws IOException {

        if(model.getTestMethods().size()!=0) {

            StringBuilder sb = new StringBuilder("public class " + driverClassName + "{\n");
            sb.append("public static void main(String[] args) throws Throwable{\n");
            sb.append("try{\n");
            // System.out.println(targetClassName + "-"+driverClassName);
            if(model.getParameterizedMethod()!=null){

                if(model.getConstructorParam().size()==1) {
                    sb.append("for(Object data:" + targetClassName + "." + model.getParameterizedMethod() + "()){\n");
                    sb.append(targetClassName + " driver= new " + targetClassName + "("+model.getConstructorParam().get(0)+"data);" );
                }else {
                    sb.append("for(int i=0;i<" + targetClassName + "." + model.getParameterizedMethod() + "().size();i++){\n");
                    sb.append("Object[][] data=(Object[][])"+targetClassName + "." + model.getParameterizedMethod() +"().toArray();\n");
                    sb.append(targetClassName + " driver= new " + targetClassName + "(");

                    for (int i = 0; i < model.getConstructorParam().size(); i++) {
                        //the last parameter
                        if (i == model.getConstructorParam().size() - 1) {
                            sb.append(model.getConstructorParam().get(i) + "data[i][" + i + "]");
                        } else {
                            sb.append(model.getConstructorParam().get(i) + "data[i][" + i + "],");
                        }

                    }
                    sb.append(");\n");
                }
            }else{
                //init statement
                sb.append(targetClassName + " driver= new " + targetClassName + "();\n");
            }

            if (model.getBeforeClass() != null) {
                sb.append("driver." + model.getBeforeClass() + "();\n");
            }


            for (String testMethod : model.getTestMethods()) {
                sb.append("\ttry{\n");
                if (model.getBefore() != null) {
                    sb.append("\tdriver." + model.getBefore() + "();\n");
                }
                sb.append("\tdriver." + testMethod + "();\n");
                if (model.getAfter() != null) {
                    sb.append("\tdriver." + model.getAfter() + "();\n");
                }
                sb.append("\t}catch(Throwable e){System.err.println(e.getMessage());}\n");
            }

            if (model.getAfterClass() != null) {
                sb.append("driver." + model.getAfterClass() + "();\n");
            }
            if(model.getParameterizedMethod()!=null){
                sb.append("}\n");
            }
            sb.append("}catch(Throwable e){System.err.println(e.getMessage());}\n");
            sb.append("}}");


            return sb;
        }
        return null;
    }
    private static void writeDriverToFile(StringBuilder sb, String driverClassName) throws IOException {
        FileUtils.writeStringToFile(new File(outputDir + "/" + driverClassName + ".java"), sb.toString(), Charset.defaultCharset());
    }

    private static String parameterBoxing(String input){
        String cast="(java.lang.Object)";
        if(input.equals("int")){
            cast="(Integer)";
        }else
        if(input.equals("double")){
            cast= "(Double)";
        }else
        if(input.equals("float")){
            cast= "(Float)";
        }else
        if(input.equals("byte")){
            cast= "(Byte)";
        }else
        if(input.equals("char")){
            cast= "(Character)";
        }else
        if(input.equals("long")){
            cast= "(Long)";
        }else
        if(input.equals("short")){
            cast= "(Short)";
        }else
        if(input.equals("boolean")){
            cast= "(Boolean)";
        }
        return cast;
    }

    private static void anaylseFieldAnnotation(CtClass clazz, JUnitModel model){
        CtField[] fields=clazz.getDeclaredFields();
        for(CtField field: fields){
            FieldInfo finfo=field.getFieldInfo();
            AnnotationsAttribute fAttr=(AnnotationsAttribute) finfo.getAttribute(AnnotationsAttribute.visibleTag);
            if(fAttr!=null) {

            }

        }
    }


    public static void anaylseJUnit3Method(CtClass clazz, JUnitModel model)  {
        CtMethod[] methods = clazz.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            CtMethod method = methods[i];
            if (method.getName().equals("setUp") && Modifier.isPublic(method.getModifiers())) {
                model.setBefore(method.getName());
            }

            if (method.getName().equals("tearDown") && Modifier.isPublic(method.getModifiers())) {
                model.setAfter(method.getName());
            }

            if(method.getName().startsWith("test") && method.getSignature().equals("()V") && Modifier.isPublic(method.getModifiers())){
                model.addTestMethod(method.getName());
            }
        }
    }
    public static void anaylseMethodAnnotation(CtClass clazz, JUnitModel model)  {
        CtMethod[] methods = clazz.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            CtMethod method = methods[i];
            MethodInfo minfo = method.getMethodInfo();
            AnnotationsAttribute methodAttr = (AnnotationsAttribute) minfo.getAttribute(AnnotationsAttribute.visibleTag);

            if (methodAttr != null) {
                if (matchTestAnnotation(methodAttr, "@org.junit.Test", "@org.junit.jupiter.api.Test") && Modifier.isPublic(method.getModifiers()) ) {
                    model.addTestMethod(method.getName());
                    recordTestAsGraphMethod(clazz, method);
                }
                if (matchTestAnnotation(methodAttr, "@org.junit.After", "@org.junit.jupiter.api.AfterEach") && Modifier.isPublic(method.getModifiers())) {
                    model.setAfter(method.getName());
                }
                if (matchTestAnnotation(methodAttr, "@org.junit.Before", "@org.junit.jupiter.api.BeforeEach") && Modifier.isPublic(method.getModifiers())) {
                    model.setBefore(method.getName());
                }
                if (matchTestAnnotation(methodAttr, "@org.junit.BeforeClass", "@org.junit.jupiter.api.BeforeAll") && Modifier.isPublic(method.getModifiers())) {
                    model.setBeforeClass(method.getName());
                }
                if (matchTestAnnotation(methodAttr, "@org.junit.AfterClass", "@org.junit.jupiter.api.AfterAll") && Modifier.isPublic(method.getModifiers())) {
                    model.setAfterClass(method.getName());
                }
                if(methodAttr.toString().startsWith("@org.junit.runners.Parameterized") && Modifier.isPublic(method.getModifiers())){
                    model.setParameterizedMethod(method.getName());
                    //a Parameterized test can only have one constructor
                    CtConstructor constructor=clazz.getDeclaredConstructors()[0];
                    try {
                        for (CtClass paraType : constructor.getParameterTypes()) {
                            String paraTypeName=paraType.getName().replace("$",".");
                            model.addConstructorParam("("+paraTypeName+")");
                        }
                    }catch(NotFoundException e){
                        System.err.println("----------------------------");
                        System.err.println("Can not resolve "+e.getMessage() +" in constructor:"+clazz.getName() );
                        System.err.println("----------------------------");
                    }
                }
            }
        }
    }

    private static void recordTestAsGraphMethod(CtClass clazz, CtMethod method) {
        try {
            String packageName = clazz.getPackageName() == null ? "" : clazz.getPackageName();
            String className = clazz.getSimpleName();
            String returnType = method.getReturnType().getName();
            String methodName = method.getName();
            String[] methodParameters = Arrays.stream(method.getParameterTypes())
                    .map(p -> p.getName())
                    .toArray(String[]::new);

            GraphMethod graphMethod = new GraphMethod(packageName, className, returnType, methodName, methodParameters);
            testMethodSet.add(graphMethod);
        }
        catch (NotFoundException e) {
            LOGGER.error("Error extracting info about test case: {}", e);
            System.exit(1);
        }
    }

    private static boolean matchTestAnnotation(AnnotationsAttribute methodAttr, String junit4, String junit5) {
        String toMatch = methodAttr.toString();
        return Util.canMatchRegex(toMatch, junit4) || Util.canMatchRegex(toMatch, junit5);
    }

    private static void removeAnnotations(CtClass clazz) throws CannotCompileException{

        AnnotationsAttribute clazzAttr=(AnnotationsAttribute)clazz.getClassFile().getAttribute(AnnotationsAttribute.visibleTag);
        if(clazzAttr!=null) {

            clazzAttr.removeAnnotation("org.junit.runner.RunWith");
            clazzAttr.removeAnnotation("org.junit.experimental.categories.Category");
            clazzAttr.removeAnnotation("org.junit.experimental.categories.Categories$IncludeCategory");
            clazzAttr.removeAnnotation("org.junit.experimental.categories.Categories.Categories$ExcludeCategory");
            clazzAttr.removeAnnotation("org.junit.Ignore");
        }

        CtField[] fields=clazz.getDeclaredFields();
        for(CtField field: fields){
            FieldInfo finfo=field.getFieldInfo();
            AnnotationsAttribute fAttr=(AnnotationsAttribute) finfo.getAttribute(AnnotationsAttribute.visibleTag);
            if(fAttr!=null) {
                fAttr.removeAnnotation("org.junit.Rule");
                fAttr.removeAnnotation("org.junit.runners.Parameterized$Parameters");
            }

        }

        CtMethod[] methods = clazz.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            CtMethod method = methods[i];
            MethodInfo minfo = method.getMethodInfo();
            AnnotationsAttribute methodAttr = (AnnotationsAttribute)minfo.getAttribute(AnnotationsAttribute.visibleTag);
            //remove method annotation
            if (methodAttr != null) {
                methodAttr.removeAnnotation("org.junit.Test");
                methodAttr.removeAnnotation("org.junit.After");
                methodAttr.removeAnnotation("org.junit.Before");
                methodAttr.removeAnnotation("org.junit.BeforeClass");
                methodAttr.removeAnnotation("org.junit.BeforeClass");
                methodAttr.removeAnnotation("org.junit.AfterClass");
                methodAttr.removeAnnotation("org.junit.Ignore");
                methodAttr.removeAnnotation("org.junit.runners.Parameterized$Parameters");//Since 4.12-beta-3


                //System.out.println(clazz.getName()+" "+methodAttr);
            }
            method.instrument(new ExprEditor() {
                @Override
                public void edit(MethodCall m) throws CannotCompileException {
                    //remove reference to junit
                    if (m.getClassName().startsWith("org.junit.Assert") || m.getClassName().startsWith("org.junit.Assume") || m.getClassName().startsWith("junit.framework")) {
                        m.replace("");
                    }
                }
            });
        }
    }

}