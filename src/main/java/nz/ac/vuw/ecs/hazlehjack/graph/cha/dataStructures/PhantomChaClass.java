package nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures;

import java.util.stream.Collectors;

public class PhantomChaClass extends ChaClass {
    public PhantomChaClass(String packageName, String className) {
        super(packageName, className);
    }

    public static ChaClass of(ChaClass clazz) {
        if (clazz.hasParent()) {
            throw new IllegalArgumentException("Phantom classes cannot have parents");
        }

        PhantomChaClass phantomClass = new PhantomChaClass(clazz.packageName, clazz.className);
        phantomClass.children = clazz.children;

        return phantomClass;
    }

    @Override
    public boolean isPhantomClass() {
        return true;
    }

    @Override
    public String toString() {
        String parentClass = parent == null ? "null" : parent.getFullName();

        return "PhantomChaClass{" +
                "packageName='" + packageName + '\'' +
                ", className='" + className + '\'' +
                ", parent=" + parentClass +
                ", children=" + children.stream()
                .map(ChaClass::getFullName)
                .collect(Collectors.joining(", ", "[", "]")) +
                '}';
    }
}
