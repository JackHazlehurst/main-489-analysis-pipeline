package nz.ac.vuw.ecs.hazlehjack.graph;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.Analyser;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Method;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static nz.ac.vuw.ecs.hazlehjack.graph.utils.Regex.METHOD_SIGNATURE_REGEX;

public class CallGraphParser {
    private static final Logger LOGGER = LogManager.getLogger(CallGraphParser.class);
    private static final String CSV_SEPARATOR = "\t";
    private static int errors = 0;

    public static Graph<GraphMethod, DefaultEdge> parseCha(Graph<GraphMethod, DefaultEdge> graph, Path toAnalyse) {
        return CallGraphParser.parseCha(graph, new Analyser().analyse(toAnalyse));
    }

    public static Graph<GraphMethod, DefaultEdge> parseCha(Graph<GraphMethod, DefaultEdge> graph, Collection<Pair<Method, Method>> chaEdges) {
        chaEdges.forEach(e -> addToGraph(graph, GraphMethod.of(e.getKey()), GraphMethod.of(e.getValue())));
        return graph;
    }

    public static Graph<GraphMethod, DefaultEdge> parse(final Path filePath) {
        errors = 0;
        Graph<GraphMethod, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);

        try {
            Files.newBufferedReader(filePath)
                    .lines()
                    .filter(CallGraphParser::canParseLine)
                    .map(CallGraphParser::parseLine)
                    .forEach(c -> addToGraph(graph, c));
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (IndexOutOfBoundsException e) {
            LOGGER.error("Error parsing file, file does not meet assumptions", e);
        }

        LOGGER.error(errors + " errors parsing this file");

        return graph;
    }

    private static void addToGraph(Graph<GraphMethod, DefaultEdge> graph, GraphMethod...classes) {
        addToGraph(graph, classes[0], classes[1]);
    }

    private static void addToGraph(Graph<GraphMethod, DefaultEdge> graph, GraphMethod source, GraphMethod target) {
        if (!graph.containsVertex(source)) {
            graph.addVertex(source);
        }
        if (!graph.containsVertex(target)) {
            graph.addVertex(target);
        }
        graph.addEdge(source, target);
    }

    private static GraphMethod[] parseLine(String line) {
        String[] splitLine = line.split(CSV_SEPARATOR);
        String invokingMethod = extractMethodSignature(splitLine[1]); //TODO: use rest of call site to verify input
        String methodBeingInvoked = extractMethodSignature(splitLine[3]);

        return new GraphMethod[]{
                GraphMethod.of(invokingMethod),
                GraphMethod.of(methodBeingInvoked)
        };
    }

    private static boolean canParseLine(String line) {
        try {
            parseLine(line);
            return true;
        }
        catch (IllegalArgumentException e) {
            errors++;
            LOGGER.warn("Failed to parse line: " + line);
            return false;
        }
    }

    /**
     * Will return the first matching method signature
     * @param column example input: "<register-finalize <sun.misc.ExtensionDependency$1: java.util.jar.Manifest run()>/<sun.misc.ExtensionDependency$1: java.util.jar.Manifest run()>/new java.util.jar.JarFile/0  >"
     * @return example output: "<sun.misc.ExtensionDependency$1: java.util.jar.Manifest run()>"
     */
    public static String extractMethodSignature(String column) {
        Pattern pattern = Pattern.compile(METHOD_SIGNATURE_REGEX);
        Matcher matcher = pattern.matcher(column);

        if (matcher.find()) {
            return matcher.group(0);
        }
        else {
            throw new IllegalArgumentException("Method signature could not be found in: " + column);
        }
    }
}
