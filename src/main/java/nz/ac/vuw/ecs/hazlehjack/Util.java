package nz.ac.vuw.ecs.hazlehjack;

import org.apache.commons.lang.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.AbstractBaseGraph;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class Util {
    private static final Logger LOGGER = LogManager.getLogger(Util.class);

    /**
     * Sudo will be prefixed to the command if running on linux
     * @return
     */
    public static Process runProcessAsRootOnLinux(String command, String errorMsg, File dir) {
        if (SystemUtils.IS_OS_LINUX) {
            LOGGER.info("Linux detected, running as sudo");
            command = "sudo " + command;
        }
        return runProcess(command, errorMsg, dir);
    }

    public static Process runProcess(String command, String errorMsg, File dir) {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec(command, null, dir);

            process.waitFor();

            return process;
        } catch (IOException | InterruptedException e) {
            LOGGER.error(errorMsg + e);
            System.exit(1);
        }
        throw new IllegalStateException(errorMsg);
    }

    /**
     *
     * @param toClone if not instance of AbstractBaseGraph ClassCastException will be thrown
     * @param <V>
     * @param <E>
     * @return
     */
    public static <V, E> Graph<V, E> cloneGraph (Graph<V, E> toClone) {
        return (Graph<V, E>) ((AbstractBaseGraph)toClone).clone();
    }

    public static String getHash(Path path) {
        if (path == null) {
            throw new IllegalArgumentException("Null is an invalid input");
        }

        return nz.ac.vuw.ecs.hazlehjack.graph.cha.Util.extractRegex(path.toString(), ".*(\\w{40}).*")
                .orElseThrow(() -> new IllegalArgumentException("Invalid path, could not match hash"));
    }
}
