package nz.ac.vuw.ecs.hazlehjack.unreflector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * This entry point is designed to be used from within the Docker container.
 */
public class StaticAnalysisDocker {
    private static final Logger LOGGER = LogManager.getLogger(StaticAnalysisDocker.class);
    private static final String DATA_DIR = "/data";
    private static final String CODE_DIR = "./code";
    private static final String INPUT_DIR = CODE_DIR + "/input";
    private static final String OUTPUT_DIR = CODE_DIR + "/output";
    private static final String JAR_EXT = "jar";

    public static void main(String[] args) {
        try (Stream<Path> paths = Files.walk(Paths.get(DATA_DIR))) {
            Path[] pathArray = paths
                    .filter(f -> {
                        final String[] fileSplit = f.getFileName().toString().split("\\.");
                        final String ext = fileSplit[fileSplit.length - 1].toLowerCase();
                        return ext.equals(JAR_EXT);
                    })
                    .sorted()
                    .toArray(Path[]::new);

            if (pathArray.length == 0) {
                LOGGER.error("Could not find jar file in " + DATA_DIR);
                System.exit(1);
            }
            else if (pathArray.length > 1) {
                LOGGER.warn(pathArray.length + " jar files detected");
            }

            Files.copy(pathArray[0], Paths.get(INPUT_DIR));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
