package nz.ac.vuw.ecs.hazlehjack.graph;

import com.google.common.collect.ImmutableList;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.Util;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Method;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.Bytecode;

import java.io.Serializable;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static nz.ac.vuw.ecs.hazlehjack.graph.utils.Regex.*;

public class GraphMethod implements Serializable {
    public static final GraphMethod EMPTY = new GraphMethod("", "", "", "");
    public final String packageName;
    public final String className;
    public final String returnType;
    public final String methodName;
    public final Set<String> annotations;
    public final ImmutableList<String> methodParameters;

    public GraphMethod(String packageName, String className, String returnType, String methodName, String...methodParameters) {
        this.packageName = packageName;
        this.className = className;
        this.returnType = returnType;
        this.methodName = methodName;
        this.annotations = Collections.unmodifiableSet(Collections.EMPTY_SET);
        this.methodParameters = ImmutableList.copyOf(methodParameters);
    }

    private GraphMethod(String packageName, String className, String returnType, String methodName, Set<String> annotations, String...methodParameters) {
        this.packageName = packageName;
        this.className = className;
        this.returnType = returnType;
        this.methodName = methodName;
        this.annotations = annotations;
        this.methodParameters = ImmutableList.copyOf(methodParameters);
    }

    public GraphMethod(String owner, String name, String descriptor) {
        String[] ownerArray = owner.split("/");

        if (ownerArray.length == 0) {
            throw new IllegalArgumentException("Invalid owner provided");
        }
        else if (ownerArray.length == 1) {
            this.packageName = "";
            this.className = ownerArray[0];
        }
        else {
            this.packageName = Arrays.stream(Arrays.copyOfRange(ownerArray, 0, ownerArray.length - 1))
                    .collect(Collectors.joining("."));
            this.className = ownerArray[ownerArray.length - 1];
        }

        this.returnType = ofHelper(descriptor, BC_RETURN_TYPE);
        this.methodName = name;

        List<String> argumentList = new ArrayList<>();
        String arguments = ofHelper(descriptor, BC_ARGUMENTS);

        while (!arguments.isEmpty()) {
            String firstChar = String.valueOf(arguments.charAt(0));

            if (Bytecode.isPrimitiveDescriptor(firstChar)) {
                argumentList.add(firstChar);
                arguments = arguments.substring(1);
            }
            else {
                String classArgument = ofHelper(arguments, BC_CLASS_ARGUMENT);
                argumentList.add(classArgument);
                arguments = arguments.substring(classArgument.length() + 2);
            }
        }

        this.annotations = Collections.unmodifiableSet(Collections.EMPTY_SET);
        this.methodParameters = ImmutableList.copyOf(argumentList);
    }

    public GraphMethod (String returnType, String[] methodParameters) {
        this.packageName = "";
        this.className = "";
        this.returnType = returnType;
        this.methodName = "";
        this.annotations = Collections.unmodifiableSet(Collections.EMPTY_SET);
        this.methodParameters = ImmutableList.copyOf(methodParameters);
    }

    public static GraphMethod of(String packageName,
                                 String className,
                                 String returnType,
                                 String methodName,
                                 String... methodParameters) {
        return new GraphMethod(packageName, className, returnType, methodName, methodParameters);
    }

    public static GraphMethod of(String packageName,
                                 String className,
                                 String returnType,
                                 String methodName,
                                 Set<String> annotations,
                                 String...methodParameters) {
        return new GraphMethod(packageName, className, returnType, methodName, annotations, methodParameters);
    }

    public static GraphMethod of(Method method) {
        return new GraphMethod(method.getPackageName(),
                method.getClassName(),
                method.returnType,
                method.methodName,
                method.annotations,
                method.getMethodParameters());
    }

    public static GraphMethod of(String owner, String name, String descriptor) {
        return new GraphMethod(owner, name, descriptor);
    }

    /**
     *
     * @param signature expects a string in the format of
     *                  "<package.ClassName: return-type method-name(param-type,param-type)>"
     *                  For example: "<calc.Calculator: int add(int,int)>"
     *                  There can be any number of parameters, from 0 upwards.
     * @return
     */
    public static GraphMethod of(String signature) throws IllegalArgumentException {
        return new GraphMethod(
                ofHelper(signature, PACKAGE_REGEX),
                ofHelper(signature, CLASS_REGEX),
                ofHelper(signature, RETURN_REGEX),
                ofHelper(signature, METHOD_REGEX),
                ofArgHelper(signature, PARAM_REGEX)
        );
    }

    private static String ofHelper(String signature, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(signature);

        if (matcher.find()) {
            return matcher.group(1);
        }
        else { //Expected not to find match e.g. when no package specified
            return "";
        }
    }

    private static String[] ofArgHelper(String signature, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(signature);
        List<String> matches = new ArrayList<>();

        while (matcher.find()) {
            matches.add(matcher.group(1));
        }

        return matches.toArray(new String[0]);
    }

    public boolean isDefaultPackage() {
        return "".equals(packageName);
    }

    public boolean hasTestAnnotation() {
        return annotations.contains("org.junit.Test") || annotations.contains("org.junit.jupiter.api.Test");
    }

    public String getQualifiedMethodName() {
        return String.format("%s %s", getQualifiedClassName(), methodName);
    }

    public String getQualifiedClassName() {
        if("".equals(packageName)) {
            return className;
        }

        return String.format("%s.%s", packageName, className);
    }

    /**
     * Example output: void assertTrue(boolean,java.lang.String) -> (boolean,java.lang.String)void
     * Special case, constructors do not output a return type: no-arg constructor -> ()
     * Special case, arrays remain the same type as they are in ASM: boolean[] -> [Z
     * @return the descriptor of the same format that the tracked invocations csv is output in
     *
     */
    public String toTrackedInvocationsDescriptor() {
        String descriptor = methodParameters.stream()
                .map(this::arrayDescriptorToAsm)
                .collect(Collectors.joining(",", "(", ")"));

        if (!"<init>".equals(methodName)) {
            descriptor += arrayDescriptorToAsm(returnType);
        }
        return descriptor;
    }

    /**
     * Will return input if not a valid array.
     * Assumes all brackets match
     * @param toFormat
     * @return
     */
    private String arrayDescriptorToAsm(String toFormat) {
        if (!toFormat.contains("[") && !toFormat.contains("]")) {
            return toFormat;
        }

        int openBracketsCount = (int) toFormat.chars().filter(c -> c == '[').count();
        int closeBracketsCount = (int) toFormat.chars().filter(c -> c == ']').count();

        if (openBracketsCount != closeBracketsCount) {
            throw new IllegalArgumentException("Number of brackets do not match");
        }

        toFormat = toFormat.substring(0, toFormat.length() - openBracketsCount * 2);

        char[] charArray = new char[openBracketsCount];
        Arrays.fill(charArray, '[');
        String formatted = new String(charArray);

        if (toFormat.equals("void")) {
            return formatted + "V";
        }
        else if (toFormat.equals("boolean")) {
            return formatted + "Z";
        }
        else if (toFormat.equals("char")) {
            return formatted + "C";
        }
        else if (toFormat.equals("byte")) {
            return formatted + "B";
        }
        else if (toFormat.equals("short")) {
            return formatted + "S";
        }
        else if (toFormat.equals("int")) {
            return formatted + "I";
        }
        else if (toFormat.equals("float")) {
            return formatted + "F";
        }
        else if (toFormat.equals("long")) {
            return formatted + "J";
        }
        else if (toFormat.equals("double")) {
            return formatted + "D";
        }

        // Assume it must be a class
        return formatted + "L" + toFormat + ";";
    }

    @Override
    public String toString() {
        return "Class{" +
                "packageName='" + packageName + '\'' +
                ", className='" + className + '\'' +
                ", returnType='" + returnType + '\'' +
                ", methodName='" + methodName + '\'' +
                ", methodParameters=" + methodParameters +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GraphMethod that = (GraphMethod) o;

        if (packageName != null ? !packageName.equals(that.packageName) : that.packageName != null) return false;
        if (className != null ? !className.equals(that.className) : that.className != null) return false;
        if (returnType != null ? !returnType.equals(that.returnType) : that.returnType != null) return false;
        if (methodName != null ? !methodName.equals(that.methodName) : that.methodName != null) return false;
        return methodParameters != null ? methodParameters.equals(that.methodParameters) : that.methodParameters == null;
    }

    @Override
    public int hashCode() {
        int result = packageName != null ? packageName.hashCode() : 0;
        result = 31 * result + (className != null ? className.hashCode() : 0);
        result = 31 * result + (returnType != null ? returnType.hashCode() : 0);
        result = 31 * result + (methodName != null ? methodName.hashCode() : 0);
        result = 31 * result + (methodParameters != null ? methodParameters.hashCode() : 0);
        return result;
    }
}
