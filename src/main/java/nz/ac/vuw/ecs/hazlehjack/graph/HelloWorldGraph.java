package nz.ac.vuw.ecs.hazlehjack.graph;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.util.mxCellRenderer;
import org.jgrapht.Graph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultUndirectedGraph;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class HelloWorldGraph {
    private static final String GRAPH_DIR = System.getProperty("user.dir") + "/src/main/resources/graph-images/";

    public static void main(String[] args) {
        HelloWorldGraph hwg = new HelloWorldGraph();

        try {
            hwg.visualiseGraph(hwg.helloGraph());
        } catch (IOException e) {
            System.err.println("Unable to visualise graph :(");
            e.printStackTrace();
        }
    }

    private Graph<String, DefaultEdge> helloGraph() {
        Graph<String, DefaultEdge> g = new DefaultUndirectedGraph<>(DefaultEdge.class);

        g.addVertex("v1");
        g.addVertex("v2");
        g.addVertex("v3");
        g.addEdge("v1", "v2");
        g.addEdge("v2", "v3");

        return g;
    }

    private void visualiseGraph(Graph<String, DefaultEdge> g) throws IOException {
        JGraphXAdapter<String, DefaultEdge> graphAdapter =
                new JGraphXAdapter<>(g);
        mxIGraphLayout layout = new mxCircleLayout(graphAdapter);
        layout.execute(graphAdapter.getDefaultParent());

        BufferedImage image = mxCellRenderer.createBufferedImage(graphAdapter, null, 2, Color.WHITE, true, null);
        File imgFile = new File(GRAPH_DIR + "hello-graph.png");
        ImageIO.write(image, "PNG", imgFile);
    }
}