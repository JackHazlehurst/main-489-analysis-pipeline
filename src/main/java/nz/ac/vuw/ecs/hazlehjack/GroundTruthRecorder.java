package nz.ac.vuw.ecs.hazlehjack;

import nz.ac.vuw.ecs.hazlehjack.dataStructures.Dependency;
import nz.ac.vuw.ecs.hazlehjack.dataStructures.Plugin;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Pair;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.GraphUtils;
import nz.ac.vuw.ecs.hazlehjack.results.AnalysisResult;
import nz.ac.vuw.ecs.hazlehjack.results.Results;
import nz.ac.vuw.ecs.hazlehjack.results.VersionResult;
import nz.ac.vuw.ecs.hazlehjack.results.rts.RTS;
import nz.ac.vuw.ecs.hazlehjack.unreflector.StaticAnalysisMain;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.revwalk.RevCommit;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class GroundTruthRecorder {
    private static final Logger LOGGER = LogManager.getLogger(GroundTruthRecorder.class);
    private static final String POM = "pom.xml";
    private static final Dependency MAVEN_DEPENDENCY = new Dependency(
            "nz.ac.vuw.ecs",
            "api-surface-recorder",
            "1.2.1",
            "test"
    );
    private static final Plugin MAVEN_PLUGIN = new Plugin(
            "org.apache.maven.plugins",
            "maven-surefire-plugin",
            "2.22.2",
            "-javaagent:aspectj-1.9.4/aspectjweaver.jar -Daj.weaving.verbose=true -Dtrackpackages=\"*\" -Dmaven.test.failure.ignore=true -Dmaven.test.error.ignore=true"
    );

    private final URL REMOTE;
    private final Path ROOT;
    private final Path REPO;
    private final ObjectId COMMIT_HASH;
    private final int NUM_COMMITS;

    /**
     *
     * @param args url to clone repo, dir path to clone repo, commit hash, number commits
     *             example: https://github.com/apache/commons-fileupload.git /Users/jackh/src/doop/auto-ground-truth-recorder/repos c27d4d1ecae76e41579b02892a7a0c27da6e34b0 10
     */
    public static void main(String[] args) {
        LOGGER.debug("New analysis starting");
        if (args.length != 4) {
            LOGGER.fatal("Arguments not provided");
            System.exit(1);
        }

        // Clone git repo, checkout working trees, modify poms, initialise results
        GroundTruthRecorder recorder = new GroundTruthRecorder(args[0], args[1], args[2], args[3]);
        List<Path> workTrees = recorder.checkOutWorkingTrees(args[0], args[1], args[2], args[3]);
        recorder.addDependencies(workTrees);
        workTrees.forEach(p -> Results.createResultRecord(Util.getHash(p)));

        // Dynamic analysis
        workTrees.forEach(p -> Results.recordDynamicAnalysis(Util.getHash(p), () -> dynamicAnalysis(p)));

        // DOOP analysis
        workTrees.forEach(GroundTruthRecorder::doopAnalysis);

        // CHA analysis
        workTrees.subList(1, workTrees.size()).stream()
                .map(Path::toString)
                .map(StaticAnalysisMain::compileClasses)
                .map(m -> m.UNREFLECTOR_INPUT)
                .forEach(p -> Results.recordChaAnalysis(Util.getHash(p), p));

        // Record results
        outputResults(Paths.get(args[1]));
    }

    public GroundTruthRecorder(String remote, String root, String hash, String num) {
        try {
            this.REMOTE = new URL(remote);
            this.ROOT = Paths.get(root);
            this.REPO = Paths.get(this.ROOT.toString(), getRepoName(this.REMOTE));
            this.COMMIT_HASH = ObjectId.fromString(hash);
            this.NUM_COMMITS = Integer.valueOf(num);

            return;
        } catch (MalformedURLException e) {
            LOGGER.error("Invalid url provided " + e);
            System.exit(1);
        }
        throw new IllegalStateException("Error initialising repo info");
    }

    public List<Path> checkOutWorkingTrees(String remote, String root, String hash, String num) {
        LOGGER.info("Cloning {} to {}", REMOTE, REPO);

        try (Git git = Git.cloneRepository()
                .setURI(REMOTE.toString())
                .setDirectory(REPO.toFile())
                .call()) {
            GroundTruthRecorder recorder = new GroundTruthRecorder(remote, root, hash, num);

            List<Path> pathList = recorder.createWorkingTrees(git);
            return pathList;
        }
        catch (GitAPIException e) {
            LOGGER.error("Could not clone repo", e);
            System.exit(1);
        }
        finally {
            Git.shutdown();
        }
        throw new IllegalStateException("State not possible");
    }

    public static Path dynamicAnalysis(Path path) {
        Util.runProcessAsRootOnLinux("mvn test --fail-never", "Error running dynamic analysis", path.toFile());
        FileFilter fileFilter = new WildcardFileFilter("tracked-invocations-*.csv");
        File[] files = path.toFile().listFiles(fileFilter);

        if (files.length != 1) {
            LOGGER.error("Could not find tracked invocations file");
            System.exit(1);
        }

        return files[0].toPath();
    }

    public static void doopAnalysis(final Path path) {
        final String workTreePath = path.toString();

        Supplier<StaticAnalysisMain> doopAnalysisSupplier = () ->
                StaticAnalysisMain.mainDoopAnalysis(new String[]{workTreePath});
        Results.recordDoopAnalysis(nz.ac.vuw.ecs.hazlehjack.Util.getHash(Paths.get(workTreePath)), doopAnalysisSupplier);
    }

    public static void outputResults(Path path) {
        Path timesTxt = Paths.get(path.toString(), "times.txt");
        try {
            Path selectedTestsDir = Paths.get(path.toString(), "selected-tests");
            LOGGER.info("Writing selected tests to: {}", selectedTestsDir);
            outputSelectedTests(selectedTestsDir);

            LOGGER.info("Collecting results: {}", path);

            for (int i = 0; i < Results.versionResultList.size(); i++) {
                VersionResult versionResult = Results.versionResultList.get(i);

                // Copy tracked invocations across
                Path from = versionResult.dynamicAnalysis.resource;
                if (from.toFile().exists()) {
                    Path to = Paths.get(path.toString(), i + "-dynamic-analysis.csv");
                    FileUtils.copyFile(from.toFile(), to.toFile());
                }
                else {
                    LOGGER.warn("Dynamic analysis not present to output");
                }

                // Output graphs
                if (!AnalysisResult.emptyPair.equals(versionResult.chaAnalysis)) {
                    Graph<GraphMethod, DefaultEdge> chaAnalysis = versionResult.chaAnalysis.result;
                    Path chaPath = Paths.get(path.toString(), i + "-cha-analysis.csv");
                    GraphUtils.testsToCsv(chaAnalysis,
                            chaPath,
                            versionResult.optionalTestSet.orElseThrow(() -> new IllegalStateException("Test methods were not recorded")));
                }
                else if (!AnalysisResult.emptyPair.equals(versionResult.staticAnalysis)) {
                    // Output the static analysis graph instead
                    Graph<GraphMethod, DefaultEdge> staticAnalysis = versionResult.staticAnalysis.result;
                    Path staticPath = Paths.get(path.toString(), i + "-cha-analysis.csv");
                    GraphUtils.testsToCsv(staticAnalysis,
                            staticPath,
                            versionResult.optionalTestSet.orElseThrow(() -> new IllegalStateException("Test methods were not recorded")));
                }
                else {
                    LOGGER.warn("CHA analysis not present to output");
                }

                if (!AnalysisResult.emptyPair.equals(versionResult.staticAnalysis)) {
                    Graph<GraphMethod, DefaultEdge> staticAnalysis = versionResult.staticAnalysis.result;
                    Path staticPath = Paths.get(path.toString(), i + "-static-analysis.csv");
                    GraphUtils.testsToCsv(staticAnalysis,
                            staticPath,
                            versionResult.optionalTestSet.orElseThrow(() -> new IllegalStateException("Test methods were not recorded")));
                }
                else {
                    LOGGER.warn("Static analysis not present to output");
                }
            }

            LOGGER.info("Writing times to: {}", timesTxt);
            FileUtils.writeStringToFile(timesTxt.toFile(), Results.toTimes(), Charset.defaultCharset());
        }
        catch (IOException e) {
            LOGGER.error("Error writing to file.... :(");
            System.exit(1);
        }
    }

    private static void outputSelectedTests(Path dir) throws IOException {
        List<RTS.SelectedTests> selectedTestsList = RTS.selectTests(Results.versionResultList);

        for (int i = 0; i < selectedTestsList.size(); i++) {
            Path file = Paths.get(dir.toString(), i + "-selected-tests.csv");
            RTS.SelectedTests tests = selectedTestsList.get(i);

            FileUtils.writeStringToFile(file.toFile(), tests.toCsv(), Charset.defaultCharset());
        }
    }

    /**
     * Will only find commit if on master branch
     * @param git
     */
    public List<Path> createWorkingTrees(Git git) {
        try {
            Iterable<RevCommit> allCommits = git.log().all().call();

            List<RevCommit> commitList = new ArrayList<>();
            RevCommit startingCommit = null;

            for (RevCommit commit : allCommits) {
                commitList.add(commit);

                if (commit.equals(COMMIT_HASH)) {
                    startingCommit = commit;
                }
            }

            if (startingCommit == null) {
                LOGGER.error("Could not find given commit: {}", COMMIT_HASH);
                System.exit(1);
            }

            //get range of relevant commits
            int commitIndex = commitList.indexOf(startingCommit);
            commitList = commitList.subList(Math.max(0, commitIndex - NUM_COMMITS + 1), commitIndex + 1);

            LOGGER.info("Creating working trees");
            List<Path> workTrees = commitList.stream()
                    .map(c -> {
                        String dirName = getRepoName(this.REMOTE) + "-" + c.name();
                        Util.runProcess(
                                String.format("git worktree add ../%s %s", dirName, c.name()),
                                "Error creating work tree :(",
                                this.REPO.toFile());

                        return Paths.get(this.ROOT.toString(), dirName);
                    })
                    .collect(Collectors.toList());

            Collections.reverse(workTrees);
            return workTrees;
        }
        catch (GitAPIException | IOException e) {
            LOGGER.error("Could not create work trees", e);
            System.exit(1);
        }
        throw new IllegalStateException("Should not be possible to get to this exception");
    }

    public void addDependencies(Collection<Path> workTrees) {
        workTrees.parallelStream()
                .map(wt -> findPomFiles(wt))
                .flatMap(Collection::stream)
                .forEach(p -> {
                    LOGGER.info("Found pom: {}", p);
                    copyAspectjJar(p.getParent());
                    PomUtil.addElementsToPom(p, MAVEN_DEPENDENCY, MAVEN_PLUGIN);
                    PomUtil.addPropertiesToPom(
                            p,
                            new Pair<>("maven.compiler.source", "1.8"),
                            new Pair<>("maven.compiler.target", "1.8"));
        });
    }

    private static void copyAspectjJar(Path destDir) {
        try {
            URL aspectjJar = Thread.currentThread().getContextClassLoader().getResource("main-resources/aspectj-1.9.4");
            FileUtils.copyDirectoryToDirectory(Paths.get(aspectjJar.getPath()).toFile(), destDir.toFile());
        } catch (IOException e) {
            LOGGER.warn("Failed to copy aspectjweaver.jar to: {}", destDir);
        }
    }

    private static Collection<Path> findPomFiles(Path dir) {
        try {
            return Files.walk(dir)
                    .filter(file -> file.endsWith(POM))
                    .collect(Collectors.toCollection(HashSet::new));
        } catch (IOException e) {
            LOGGER.warn("Could not find any pom files in {}", dir);
            return new HashSet<>();
        }
    }

    /**
     *
     * @param url
     * @return returns repo name e.g. https://github.com/apache/pdfbox.git -> pdfbox
     */
    private static String getRepoName(URL url) {
        String[] path = url.getPath().split("/");
        String pathEnd = path[path.length - 1];
        return pathEnd.split("\\.")[0];
    }
}
