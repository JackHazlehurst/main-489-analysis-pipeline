package nz.ac.vuw.ecs.hazlehjack.dataStructures;

import nz.ac.vuw.ecs.hazlehjack.PomUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.Optional;

public class Dependency extends PomElement{
    public final Optional<String> scope;

    public Dependency(String groupId, String artifactId, String version, String scope) {
        super(groupId, artifactId, version, "dependencies", "dependency");
        this.scope = Optional.of(scope);
    }

    @Override
    public Node toNode(Document document) {
        Node parent = super.toNode(document);
        scope.ifPresent(s -> PomUtil.appendChildNode(document, parent, "scope", Optional.of(s)));
        return parent;
    }
}
