package nz.ac.vuw.ecs.hazlehjack.unreflector.util;

import nz.ac.vuw.ecs.hazlehjack.unreflector.StaticAnalysisMain;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zeroturnaround.zip.ZipUtil;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Jar {
    private static final Logger LOGGER = LogManager.getLogger(Jar.class);

    public static void create(Path compiledInput, Path jarOutput, String entryPoint, String version) {
        makeManifest(compiledInput, entryPoint, version);
        ZipUtil.pack(compiledInput.toFile(), jarOutput.toFile());
    }

    private static Path makeManifest(Path compiledInput, String entryPoint, String version) {
        Path manifestFile = Paths.get(compiledInput.toString(), "META-INF", "MANIFEST.MF");

        try {
            String manifest = "Manifest-Version: 1.0\n" +
                    "Created-By: " + version + "\n" +
                    "Main-Class: " + entryPoint + "\n";

            FileUtils.writeStringToFile(manifestFile.toFile(), manifest, Charset.defaultCharset());

            return manifestFile;
        } catch (IOException e) {
            LOGGER.error("Could not create manifest {}: {}", manifestFile, e);
            System.exit(1);
        }
        throw new IllegalStateException("Unreachable code...");
    }
}
