package nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.Util;

import java.util.*;
import java.util.stream.Collectors;

import static nz.ac.vuw.ecs.hazlehjack.graph.cha.Util.*;

public class Method {
    public final ChaClass clazz;
    public final Set<String> annotations;
    public final String methodName;
    public final String returnType;
    private List<String> methodParameters;

    public Method(ChaClass clazz, String methodName, String descriptor) {
        this.clazz = clazz;
        this.annotations = Collections.unmodifiableSet(Collections.EMPTY_SET);
        this.methodName = methodName;
        this.returnType = extractReturnType(descriptor);
        this.methodParameters = extractMethodParameters(descriptor);
    }

    public Method(ChaClass clazz, String methodName, String returnType, String... parameters) {
        this.clazz = clazz;
        this.annotations = Collections.unmodifiableSet(Collections.EMPTY_SET);
        this.methodName = methodName;
        this.returnType = returnType;
        this.methodParameters = Arrays.asList(parameters);
    }

    public Method(ChaClass clazz, Method method) {
        this.clazz = clazz;
        this.annotations = method.annotations;
        this.methodName = method.methodName;
        this.returnType = method.returnType;
        this.methodParameters = method.methodParameters;
    }

    private Method(Method method, Set<String> annotations) {
        this.clazz = method.clazz;
        this.annotations = annotations;
        this.methodName = method.methodName;
        this.returnType = method.returnType;
        this.methodParameters = method.methodParameters;
    }

    public static Method of(ChaClass clazz, String methodName, String returnType, String... parameters) {
        return new Method(clazz, methodName, returnType, parameters);
    }

    public Method addAnnotations(Set<String> annotations) {
        Set<String> annotationsClone = annotations.stream().collect(Collectors.toSet());
        return new Method(this, Collections.unmodifiableSet(annotationsClone));
    }

    public String getPackageName() {
        return clazz.packageName;
    }

    public String getClassName() {
        return clazz.className;
    }

    public String[] getMethodParameters() {
        return methodParameters.toArray(new String[]{});
    }

    public static String extractReturnType(String descriptor) {
        String returnType = extractRegex(descriptor, "\\)(.*)")
                .orElseThrow(() -> new IllegalArgumentException("Could not match return type"));

        return Util.parseType(returnType);
    }

    public static List<String> extractMethodParameters(String descriptor) {
        List<String> parameterList = new ArrayList<>();
        String parameters = Util.extractRegex(descriptor, "\\((.*)\\)")
                .orElseThrow(() -> new IllegalArgumentException("Could not match parameters"));

        while (!parameters.isEmpty()) {
            if (parameters.matches("\\[*L.*")) {
                int arrayNum = getArrayNum(parameters);
                String param = parseType(parameters);

                parameters = parameters.substring(param.length() + 2 - arrayNum);
                parameterList.add(param);
            }
            else if (parameters.matches("\\[*[VZCBSIFJD].*")) {
                String param = Util.extractRegex(parameters, "(\\[*[VZCBSIFJD]).*")
                        .orElseThrow(() -> new IllegalStateException("Internal error, could not match type: " + descriptor));
                int arrayNum = getArrayNum(param);

                param = param.substring(arrayNum, arrayNum + 1);
                param = Util.parseType(param);
                param = makeArray(param, arrayNum);

                parameters = parameters.substring(1 + arrayNum);
                parameterList.add(param);
            }
            else {
                throw new IllegalArgumentException("Invalid descriptor: " + descriptor);
            }
        }

        return parameterList;
    }

    private static String getNextParam(String parameter) {
        if (parameter.startsWith("[")) {
            for (int i = 0; i < parameter.length(); i++) {
                if (parameter.charAt(i) != '[') {
                    return parameter.substring(0, i + 1);
                }
            }
            throw new IllegalStateException("Should not be able to reach this point");
        }

        return parameter.substring(0, 1);
    }

    @Override
    public String toString() {
        String clazzName = clazz == null ? "null" : clazz.getFullName();

        return "Method{" +
                "clazz=" + clazzName +
                ", methodName='" + methodName + '\'' +
                ", returnType='" + returnType + '\'' +
                ", methodParameters=" + methodParameters +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Method method = (Method) o;

        if (clazz != null ? !clazz.equals(method.clazz) : method.clazz != null) return false;
        if (methodName != null ? !methodName.equals(method.methodName) : method.methodName != null) return false;
        if (returnType != null ? !returnType.equals(method.returnType) : method.returnType != null) return false;
        return methodParameters != null ? methodParameters.equals(method.methodParameters) : method.methodParameters == null;
    }

    @Override
    public int hashCode() {
        int result = clazz != null ? clazz.hashCode() : 0;
        result = 31 * result + (methodName != null ? methodName.hashCode() : 0);
        result = 31 * result + (returnType != null ? returnType.hashCode() : 0);
        result = 31 * result + (methodParameters != null ? methodParameters.hashCode() : 0);
        return result;
    }
}
