package testPkg;

import main.Main;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class TestsInPackage {
    @Test
    public void testDoAlphabetDoesNotThrow() {
        assertDoesNotThrow(() -> new Main().doAlphabet());
    }
}
