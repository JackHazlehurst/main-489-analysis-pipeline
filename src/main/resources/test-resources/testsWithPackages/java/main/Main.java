package main;

import alphabet.A;
import alphabet.B;
import alphabet.C;
import alphabet.D;

public class Main {
    public static void main(String[] args) {
        new Main().doAlphabet();
    }

    public void doAlphabet() {
        A a = new A();
        A b = new B();
        B c = new C();
        D d = new D();

        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(c.toString());
        System.out.println(d.toString());

        System.out.println(d.getShopping());
    }
}
