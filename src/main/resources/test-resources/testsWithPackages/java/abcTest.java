import alphabet.A;
import alphabet.B;
import alphabet.C;
import alphabet.D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class abcTest {
    @Test
    public void checkBChildren() {
        assertFalse(new A() instanceof B);

        assertTrue(new B() instanceof B);
        assertTrue(new C() instanceof B);
        assertTrue(new D() instanceof B);
    }

    @Test
    public void checkNotBChildren() {
        assertFalse(new B() instanceof C);
        assertFalse(new B() instanceof D);
    }

    @Test
    public void checkShoppingList() {
        assertEquals("{apples=2, oranges=7, grapes=0}", new D().getShopping());
    }

    @Test
    public void checkNames() {
        assertEquals("I'm a", new A().toString());
        assertEquals("I'm b", new B().toString());
        assertEquals("I'm c", new C().toString());
        assertEquals("I'm d", new D().toString());// Expected to fail, D does not know its own name
    }
}