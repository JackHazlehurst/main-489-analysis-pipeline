package parsing.cha;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.Analyser;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.Util;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.ChaClass;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import parsing.ParsingCommon;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class ChaClassHierarchyTest extends ParsingCommon {
    @ParameterizedTest
    @MethodSource({"getAnalyserStream0", "getAnalyserStream1", "getAnalyserStream2"})
    public void ensureAllObjectsHaveParentsExceptObject(Analyser analyser) {
        List<ChaClass> classesWithNoParent = analyser.recordedClasses.stream()
                .filter(c -> c.parent == null)
                .filter(c -> !c.isPhantomClass())
                .collect(Collectors.toList());

        // Only Object should have no parent
        assertEquals(1, classesWithNoParent.size());
        ChaClass object = classesWithNoParent.get(0);
        assertEquals("java.lang", object.packageName);
        assertEquals("Object", object.className);
    }

    @Test
    public void ensureStandardLibraryMapClassesHaveParents() {
        ChaClass object = ChaClass.of("java.lang", "Object");
        ChaClass abstractMap = ChaClass.of("java.util", "AbstractMap");
        ChaClass hashMap = ChaClass.of("java.util", "HashMap");
        ChaClass weakHashMap = ChaClass.of("java.util", "WeakHashMap");


        Set<ChaClass> classSet = new HashSet<>();
        classSet.add(hashMap);
        classSet.add(weakHashMap);
        classSet = Analyser.getStandardLibraryParents(classSet);

        checkClassHierarchy(Util.getFromCollection(classSet, hashMap), abstractMap);
        checkClassHierarchy(Util.getFromCollection(classSet, weakHashMap), abstractMap);
        checkClassHierarchy(Util.getFromCollection(classSet, abstractMap), object, hashMap, weakHashMap);
        checkClassHierarchy(Util.getFromCollection(classSet, object), null, abstractMap);
    }

    @Test
    public void checkChaClassMerge() {
        ChaClass z = ChaClass.of("Z");
        ChaClass a = ChaClass.of("A");
        ChaClass a2 = ChaClass.of("A");
        ChaClass b = ChaClass.of("B");
        ChaClass c = ChaClass.of("C");
        ChaClass d = ChaClass.of("D");

        a.addChild(b);
        a.addChild(c);
        a2.addChild(d);

        a.setParent(z);
        a2.setParent(z);
        b.setParent(a);
        c.setParent(a);
        d.setParent(a2);

        ChaClass mergedA = ChaClass.merge(a, a2);
        checkClassHierarchy(mergedA, z, b, c, d);
    }

    @Test
    public void checkComplexChaClassMerge() {
        ChaClass a = ChaClass.of("test", "A");
        ChaClass b = ChaClass.of("test", "B");
        ChaClass a2 = ChaClass.of("test", "A");
        ChaClass b2 = ChaClass.of("test", "B");
        ChaClass c = ChaClass.of("test", "C");
        ChaClass d = ChaClass.of("test", "D");

        a.addChild(b);
        b.setParent(a);
        b.addChild(c);
        c.setParent(b);

        a2.addChild(b2);
        b2.setParent(a2);
        b2.addChild(d);
        d.setParent(b2);

        ChaClass mergedA = ChaClass.merge(a, a2);
        ChaClass mergedB = mergedA.getDirectChild(b);

        checkClassHierarchy(mergedA, null, b);
        checkClassHierarchy(mergedB, mergedA, c, d);
    }

    @Test
    public void checkCanStillMergeIfOneParentNull() {
        ChaClass a = ChaClass.of("test", "A");
        ChaClass b = ChaClass.of("test", "B");
        ChaClass bNoParent = ChaClass.of("test", "B");
        ChaClass c = ChaClass.of("test", "C");

        b.setParent(a);
        a.addChild(b);

        c.setParent(bNoParent);
        bNoParent.addChild(c);

        ChaClass mergedB = ChaClass.merge(b, bNoParent);

        checkClassHierarchy(mergedB, a, c);
    }

    @Test
    public void tryInvalidMerges() {
        ChaClass q = ChaClass.of("Q");
        ChaClass r = ChaClass.of("R");
        r.setParent(q);

        assertThrows(IllegalArgumentException.class, () -> ChaClass.merge(ChaClass.of("A"), ChaClass.of("B")));
        assertThrows(IllegalArgumentException.class, () -> ChaClass.merge(ChaClass.of("A"), ChaClass.of("alpha", "A")));
    }

    @Test
    public void ensureAlphabetHierarchyIsCorrect() {
        ChaClass object = ChaClass.of("java.lang", "Object");
        ChaClass a = ChaClass.of("A");
        ChaClass b = ChaClass.of("B");
        ChaClass c = ChaClass.of("C");
        ChaClass d = ChaClass.of("D");

        Collection<ChaClass> classes = analyser0.recordedClasses;
        checkClassHierarchy(Util.getFromCollection(classes, a), object, b);
        checkClassHierarchy(Util.getFromCollection(classes, b), a, c, d);
        checkClassHierarchy(Util.getFromCollection(classes, c), b);
        checkClassHierarchy(Util.getFromCollection(classes, d), b);
    }

    @Test
    public void ensureOtherClassHierarchyIsCorrect() {
        ChaClass object = ChaClass.of("java.lang", "Object");
        ChaClass main = ChaClass.of("Main");
        ChaClass abcTest = ChaClass.of("abcTest");

        Collection<ChaClass> classes = analyser0.recordedClasses;
        checkClassHierarchy(Util.getFromCollection(classes, main), object);
        checkClassHierarchy(Util.getFromCollection(classes, abcTest), object);
    }

    private void checkClassHierarchy(ChaClass classToCheck, ChaClass parent, ChaClass... children) {
        Set<ChaClass> childrenToCheck = classToCheck.children;

        assertEquals(parent, classToCheck.parent);
        assertEquals(children.length, childrenToCheck.size());
        Arrays.stream(children)
                .forEach(child -> childrenToCheck.contains(child));
    }
}
