package parsing.cha;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.Analyser;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.ChaClass;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Method;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Pair;
import org.junit.jupiter.api.Test;
import parsing.ParsingCommon;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaMethodEdgesTest extends ParsingCommon {
    @Test
    public void checkClassesInMethodPairsWerePreviouslyFound() {
        analyser0.recordedEdges.stream()
                .flatMap(p -> Stream.of(p.getKey(), p.getValue()))
                .forEach(m -> assertTrue(containsSameObjectOrNotAtAll(analyser0.recordedClasses, m.clazz)));
    }

    private <E> boolean containsSameObjectOrNotAtAll(Collection<E> collection, E toCheck) {
        boolean containsSameObject = collection.stream()
                .anyMatch(e -> e == toCheck);
        boolean containsClass = collection.stream()
                .anyMatch(e -> e.equals(toCheck));

        return containsSameObject == containsClass;
    }

    @Test
    public void ensureEdgesFromChildrenAreAdded() {
        Method caller = newMethod("Main", "doAlphabet", "void");
        Method callee = newMethod("A", "toString", "java.lang.String");
        Pair<Method, Method> callPair = new Pair<>(caller, callee);
        Collection<Pair<Method, Method>> callPairCollection = new HashSet<>();

        callPairCollection.add(callPair);
        callPairCollection = Analyser.getOtherEdges(analyser0.recordedClasses, callPairCollection);

        isEdgePresent(callPairCollection, newMethod("Main", "doAlphabet", "void"), newMethod("B", "toString", "java.lang.String"));
        isEdgePresent(callPairCollection, newMethod("Main", "doAlphabet", "void"), newMethod("C", "toString", "java.lang.String"));
        isEdgePresent(callPairCollection, newMethod("Main", "doAlphabet", "void"), newMethod("D", "toString", "java.lang.String"));
    }

    @Test
    public void ensureMainMethodHasAllEdges() {
        Set<Pair<Method, Method>> edges = analyser0.recordedEdges;

        isEdgePresent(edges, newMethod("Main", "doAlphabet", "void"), newMethod("A", "<init>", "void"));
        isEdgePresent(edges, newMethod("Main", "doAlphabet", "void"), newMethod("B", "<init>", "void"));
        isEdgePresent(edges, newMethod("Main", "doAlphabet", "void"), newMethod("C", "<init>", "void"));
        isEdgePresent(edges, newMethod("Main", "doAlphabet", "void"), newMethod("D", "<init>", "void"));

        isEdgePresent(edges, newMethod("Main", "doAlphabet", "void"), newMethod("A", "toString", "java.lang.String"));
        isEdgePresent(edges, newMethod("Main", "doAlphabet", "void"), newMethod("B", "toString", "java.lang.String"));
        isEdgePresent(edges, newMethod("Main", "doAlphabet", "void"), newMethod("C", "toString", "java.lang.String"));
        isEdgePresent(edges, newMethod("Main", "doAlphabet", "void"), newMethod("D", "toString", "java.lang.String"));
    }

    private void isEdgePresent(Collection<Pair<Method, Method>> edges, Method key, Method value) {
        assertTrue(edges.contains(new Pair<>(key, value)));
    }

    private Method newMethod(String clazz, String methodName, String returnType, String... parameters) {
        return Method.of(ChaClass.of(clazz), methodName, returnType, parameters);
    }
}
