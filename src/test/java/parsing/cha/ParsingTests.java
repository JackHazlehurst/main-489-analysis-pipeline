package parsing.cha;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.Util;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Method;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParsingTests {
    private static Stream<Arguments> getSampleDescriptors() {
        return Stream.of(
                Arguments.of("(Ljava/lang/String;)V", Arrays.asList("java.lang.String")),
                Arguments.of("()L", Arrays.asList()),
                Arguments.of("([Ljava/lang/String;)V", Arrays.asList("java.lang.String[]")),
                Arguments.of("(ZZJ)Ljava/lang/String;", Arrays.asList("boolean", "boolean", "long")),
                Arguments.of("(ZZJ)F", Arrays.asList("boolean", "boolean", "long")),
                Arguments.of("(ZCBSIFJD)S", Arrays.asList("boolean", "char", "byte", "short", "int", "float", "long", "double")),
                Arguments.of("(Z[CD)[F", Arrays.asList("boolean", "char[]", "double")),
                Arguments.of("(FJ[Ljava/util/List;D)J", Arrays.asList("float", "long", "java.util.List[]", "double")),
                Arguments.of("([Ljava/lang/String;JLjava/util/List;)Z", Arrays.asList("java.lang.String[]", "long", "java.util.List")),
                Arguments.of("(Lorg/junit/jupiter/params/shadow/com/univocity/parsers/common/routine/AbstractRoutines$3;Lorg/junit/jupiter/params/shadow/com/univocity/parsers/common/AbstractParser;)V",
                        Arrays.asList("org.junit.jupiter.params.shadow.com.univocity.parsers.common.routine.AbstractRoutines$3", "org.junit.jupiter.params.shadow.com.univocity.parsers.common.AbstractParser"))
        );
    }

    private static Stream<Arguments> getSampleTypes() {
        return Stream.of(
                Arguments.of("V", "void"), Arguments.of("[V", "void[]"),
                Arguments.of("Z", "boolean"), Arguments.of("[Z", "boolean[]"),
                Arguments.of("C", "char"), Arguments.of("[C", "char[]"),
                Arguments.of("B", "byte"), Arguments.of("[B", "byte[]"),
                Arguments.of("S", "short"), Arguments.of("[S", "short[]"),
                Arguments.of("I", "int"), Arguments.of("[I", "int[]"),
                Arguments.of("F", "float"), Arguments.of("[F", "float[]"),
                Arguments.of("J", "long"), Arguments.of("[J", "long[]"),
                Arguments.of("D", "double"), Arguments.of("[D", "double[]"),
                Arguments.of("L", "<class>"), Arguments.of("[L", "<class>[]"),
                Arguments.of("[[D", "double[][]"), Arguments.of("[[[D", "double[][][]"),
                Arguments.of("[[[[[[Z", "boolean[][][][][][]"),
                Arguments.of("Ljava/lang/String;", "java.lang.String"),
                Arguments.of("[Ljava/lang/String;", "java.lang.String[]"),
                Arguments.of("[[Ljava/lang/String;", "java.lang.String[][]")
        );
    }

    @ParameterizedTest
    @MethodSource("getSampleDescriptors")
    public void extractMethodDescriptors(String descriptor, List<String> expectedList) {
        assertEquals(expectedList, Method.extractMethodParameters(descriptor));
    }

    @ParameterizedTest
    @MethodSource("getSampleTypes")
    public void parseTypes(String descriptor, String expectedString) {
        assertEquals(expectedString, Util.parseType(descriptor));
    }
}
