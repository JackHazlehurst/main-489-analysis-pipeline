package parsing.cha.isolatedCha;

import nz.ac.vuw.ecs.hazlehjack.Util;
import nz.ac.vuw.ecs.hazlehjack.graph.CallGraphParser;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.Analyser;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Method;
import nz.ac.vuw.ecs.hazlehjack.graph.cha.dataStructures.Pair;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.GraphUtils;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class TestChaRealDataInIsolation {
    private static Path chaClassFiles;
    private static Graph<GraphMethod, DefaultEdge> doopGraph;

    @BeforeAll
    public static void setUp() {
        URL chaClassFilesUrl = Thread.currentThread().getContextClassLoader()
                .getResource("test-resources/isolated-cha/9e81cf2-abc-maven");
        chaClassFiles = Paths.get(chaClassFilesUrl.getPath());

        URL callGraphCsv = Thread.currentThread().getContextClassLoader()
                .getResource("test-resources/isolated-cha/CallGraphEdge-8d632df-abc-maven.csv");
        doopGraph = CallGraphParser.parse(Paths.get(callGraphCsv.getPath()));
    }

    @Test
    public void testCha() {
        Analyser analyser = new Analyser();
        Collection<Pair<Method, Method>> methodPairs = analyser.analyse(chaClassFiles);
        Graph<GraphMethod, DefaultEdge> graph = CallGraphParser.parseCha(Util.cloneGraph(doopGraph), methodPairs);

        // Need to manually add methods, skipped usual steps for time savings
        Set<GraphMethod> missedTests = new HashSet<>();
        missedTests.add(GraphMethod.of("", "abcTest", "void", "checkBChildren"));
        missedTests.add(GraphMethod.of("", "abcTest", "void", "checkNotBChildren"));

        GraphUtils.testsToCsv(graph, Paths.get(chaClassFiles.getParent().toString(), "test-cha-analysis.csv"), missedTests);
    }
}
