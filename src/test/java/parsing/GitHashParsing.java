package parsing;

import nz.ac.vuw.ecs.hazlehjack.Util;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GitHashParsing {
    private static Stream<Arguments> provideWorkTreePaths() {
        return Stream.of(
                Arguments.of(Paths.get("abc-maven-c83dcefea9ab6e2bb03473808ddebf4ed26c65ea-data"),
                        "c83dcefea9ab6e2bb03473808ddebf4ed26c65ea"),
                Arguments.of(Paths.get("abc-maven-c83dcefea9ab6e2bb03473808ddebf4ed26c65ea"),
                        "c83dcefea9ab6e2bb03473808ddebf4ed26c65ea"),
                Arguments.of(Paths.get("abc-maven-8d632dfd66a62ebaa9a95819c660bcd2a2251aaa"),
                        "8d632dfd66a62ebaa9a95819c660bcd2a2251aaa"),
                Arguments.of(Paths.get("abc-maven-8d632dfd66a62ebaa9a95819c660bcd2a2251aaa-data"),
                        "8d632dfd66a62ebaa9a95819c660bcd2a2251aaa"),
                Arguments.of(Paths.get("/Users/jackh/src/doop/auto-ground-truth-recorder/repos/abc-maven-9e81cf2a2ad552800c3ff7be9358db87d551873e/target/classes"),
                        "9e81cf2a2ad552800c3ff7be9358db87d551873e")
        );
    }

    @ParameterizedTest
    @MethodSource("provideWorkTreePaths")
    public void testHashParsing(Path path, String expectedHash) {
        assertEquals(expectedHash, Util.getHash(path));
    }

    @Test
    public void testInvalidPaths() {
        assertThrows(IllegalArgumentException.class, () -> Util.getHash(Paths.get("drt6y7ujhbvftyu")));
        assertThrows(IllegalArgumentException.class, () -> Util.getHash(Paths.get("d")));
        assertThrows(IllegalArgumentException.class, () -> Util.getHash(null));
    }
}
