package parsing.graph;

import nz.ac.vuw.ecs.hazlehjack.Util;
import nz.ac.vuw.ecs.hazlehjack.graph.CallGraphParser;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import parsing.ParsingCommon;

import java.net.URL;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class FileCallGraphParseTest extends ParsingCommon {
    private static Graph<GraphMethod, DefaultEdge> graph;
    private static Graph<GraphMethod, DefaultEdge> graphChaFirstIteration;
    private static Graph<GraphMethod, DefaultEdge> graphChaSecondIteration;

    private static final GraphMethod entryPointMain = GraphMethod.of("", "EntryPoint_builtinTest", "void", "main", "java.lang.String[]");
    private static final GraphMethod driverMain = GraphMethod.of("", "Driver_builtinTest0", "void", "main", "java.lang.String[]");

    private static final GraphMethod abcTestCheckBChildren = GraphMethod.of("", "abcTest", "void", "checkBChildren");
    private static final GraphMethod abcTestCheckNotBChildren = GraphMethod.of("", "abcTest", "void", "checkNotBChildren");
    private static final GraphMethod abcTestCheckShoppingList = GraphMethod.of("", "abcTest", "void", "checkShoppingList");
    private static final GraphMethod abcTestCheckNames = GraphMethod.of("", "abcTest", "void", "checkNames");

    private static final GraphMethod aInit = GraphMethod.of("", "A", "void", "<init>");
    private static final GraphMethod aToString = GraphMethod.of("", "A", "java.lang.String", "toString");

    private static final GraphMethod bInit = GraphMethod.of("", "B", "void", "<init>");
    private static final GraphMethod bToString = GraphMethod.of("", "B", "java.lang.String", "toString");

    private static final GraphMethod cInit = GraphMethod.of("", "C", "void", "<init>");
    private static final GraphMethod cToString = GraphMethod.of("", "C", "java.lang.String", "toString");

    private static final GraphMethod dInit = GraphMethod.of("", "D", "void", "<init>");
    private static final GraphMethod dToString = GraphMethod.of("", "D", "java.lang.String", "toString");
    private static final GraphMethod dGetShopping = GraphMethod.of("", "D", "java.lang.String", "getShopping");

    private static final GraphMethod immutableMapOf = GraphMethod.of("com.google.common.collect", "ImmutableMap", "com.google.common.collect.ImmutableMap", "of", "java.lang.Object", "java.lang.Object", "java.lang.Object", "java.lang.Object", "java.lang.Object", "java.lang.Object");
    private static final GraphMethod immutableMapToString = GraphMethod.of("com.google.common.collect", "ImmutableMap", "java.lang.String", "toString");
    private static final GraphMethod integerValueOf = GraphMethod.of("java.lang", "Integer", "java.lang.Integer", "valueOf", "int");

    private static final GraphMethod assertTrue = GraphMethod.of("org.junit.jupiter.api", "Assertions", "void", "assertTrue", "boolean");
    private static final GraphMethod assertFalse = GraphMethod.of("org.junit.jupiter.api", "Assertions", "void", "assertFalse", "boolean");
    private static final GraphMethod assertEquals = GraphMethod.of("org.junit.jupiter.api", "Assertions", "void", "assertEquals", "java.lang.Object", "java.lang.Object");

    @BeforeAll
    public static void createGraphs() {
        new ParsingCommon();// Ensure the analysis has been done before this static method is called

        URL callGraphPath = Thread.currentThread().getContextClassLoader().getResource("test-resources/call-graph-edges/CallGraphEdge.csv");
        graph = CallGraphParser.parse(Paths.get(callGraphPath.getPath()));

        graphChaFirstIteration = Util.cloneGraph(graph);
        assertEquals(graph, graphChaFirstIteration);
        CallGraphParser.parseCha(graphChaFirstIteration, analyser0.recordedEdges);

        graphChaSecondIteration = Util.cloneGraph(graphChaFirstIteration);
        assertEquals(graphChaFirstIteration, graphChaSecondIteration);
        CallGraphParser.parseCha(graphChaSecondIteration, analyser3.recordedEdges);
    }

    @Test
    public void ensureVertexesPresent() {
        Set<GraphMethod> vertexSet = graph.vertexSet().stream()
                .filter(GraphMethod::isDefaultPackage)
                .collect(Collectors.toSet());

        assertEquals(9, vertexSet.size());

        ensureClassPresent(vertexSet, "A", "void", "<init>");
        ensureClassPresent(vertexSet, "B", "void", "<init>");
        ensureClassPresent(vertexSet, "C", "void", "<init>");
        ensureClassPresent(vertexSet, "D", "void", "<init>");

        ensureClassPresent(vertexSet, "abcTest", "void", "<init>");
        ensureClassPresent(vertexSet, "abcTest", "void", "checkBChildren");
        ensureClassPresent(vertexSet, "abcTest", "void", "checkNotBChildren");

        ensureClassPresent(vertexSet, "EntryPoint_builtinTest", "void", "main", "java.lang.String[]");
        ensureClassPresent(vertexSet, "Driver_builtinTest0", "void", "main", "java.lang.String[]");
    }

    @Test
    public void ensureEdgesPresent() {
        ensureEdgePresent(graph, entryPointMain, driverMain);
        ensureEdgePresent(graph, driverMain, abcTestCheckBChildren);
        ensureEdgePresent(graph, driverMain, abcTestCheckNotBChildren);

        ensureEdgePresent(graph, abcTestCheckBChildren, aInit);
        ensureEdgePresent(graph, abcTestCheckBChildren, bInit);
        ensureEdgePresent(graph, abcTestCheckBChildren, cInit);
        ensureEdgePresent(graph, abcTestCheckBChildren, dInit);
        ensureEdgePresent(graph, abcTestCheckBChildren, assertTrue);
        ensureEdgePresent(graph, abcTestCheckBChildren, assertFalse);

        ensureEdgePresent(graph, abcTestCheckNotBChildren, bInit);
        ensureEdgePresent(graph, abcTestCheckNotBChildren, assertFalse);
    }

    @Test
    public void addChaToGraph() {
        // Ensure same edges are present as previous analysis
        ensureEdgePresent(graphChaFirstIteration, entryPointMain, driverMain);
        ensureEdgePresent(graphChaFirstIteration, driverMain, abcTestCheckBChildren);
        ensureEdgePresent(graphChaFirstIteration, driverMain, abcTestCheckNotBChildren);

        ensureEdgePresent(graphChaFirstIteration, abcTestCheckBChildren, aInit);
        ensureEdgePresent(graphChaFirstIteration, abcTestCheckBChildren, bInit);
        ensureEdgePresent(graphChaFirstIteration, abcTestCheckBChildren, cInit);
        ensureEdgePresent(graphChaFirstIteration, abcTestCheckBChildren, dInit);
        ensureEdgePresent(graphChaFirstIteration, abcTestCheckBChildren, assertTrue);
        ensureEdgePresent(graphChaFirstIteration, abcTestCheckBChildren, assertFalse);

        ensureEdgePresent(graphChaFirstIteration, abcTestCheckNotBChildren, bInit);
        ensureEdgePresent(graphChaFirstIteration, abcTestCheckNotBChildren, assertFalse);

        // Ensure the parseCha method added expected new edges
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, abcTestCheckShoppingList, dInit);
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, abcTestCheckShoppingList, dGetShopping);
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, abcTestCheckShoppingList, assertEquals);

        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, dGetShopping, immutableMapOf);
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, dGetShopping, immutableMapToString);
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, dGetShopping, integerValueOf);// Auto boxing/unboxing

        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, abcTestCheckNames, aInit);
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, abcTestCheckNames, bInit);
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, abcTestCheckNames, aToString);
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, abcTestCheckNames, bToString);
        ensureEdgePresentNotPresent(graphChaFirstIteration, graph, abcTestCheckNames, assertEquals);
    }

    /**
     * Both should be the same, even though the code is different.
     * This is because the CHA adds the newly added code as possible children methods.
     */
    @Test
    public void addSecondChaToGraph() {
        assertEquals(graphChaFirstIteration, graphChaSecondIteration);
    }

    private void ensureEdgePresent(Graph graph, GraphMethod source, GraphMethod target) {
        assertTrue(graph.containsEdge(source, target));
    }

    private void ensureEdgeNotPresent(Graph graph, GraphMethod source, GraphMethod target) {
        assertFalse(graph.containsEdge(source, target));
    }

    private void ensureEdgePresentNotPresent(Graph presentGraph,
                                             Graph notPresentGraph,
                                             GraphMethod source,
                                             GraphMethod target) {
        ensureEdgePresent(presentGraph, source, target);
        ensureEdgeNotPresent(notPresentGraph, source, target);
    }

    private void ensureClassPresent(Set<GraphMethod> classSet,
                                    String className,
                                    String returnType,
                                    String methodName,
                                    String... methodParameters) {
        GraphMethod newClass = GraphMethod.of("", className, returnType, methodName, methodParameters);
        assertTrue(classSet.contains(newClass));
    }
}
