package parsing.graph;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GraphMethodTest {
    private static Stream<Arguments> provideMethodSignatureParsingStrings() {
        return Stream.of(
                Arguments.of("<java.lang.System$2: void invokeFinalize(java.lang.Object)>",
                        new GraphMethod("java.lang",
                                "System$2",
                                "void",
                                "invokeFinalize",
                                "java.lang.Object")),
                Arguments.of("<java.lang.Object: void finalize()>",
                        new GraphMethod("java.lang",
                                "Object",
                                "void",
                                "finalize")),
                Arguments.of("<java.lang.invoke.MethodHandleImpl$IntrinsicMethodHandle: java.lang.invoke.MethodHandle asCollector(java.lang.Class,int)>",
                        new GraphMethod("java.lang.invoke",
                                "MethodHandleImpl$IntrinsicMethodHandle",
                                "java.lang.invoke.MethodHandle",
                                "asCollector",
                                "java.lang.Class", "int")),
                Arguments.of("<calc.Calculator: int sum(int,int,int,int,int,int)>",
                        new GraphMethod("calc",
                                "Calculator",
                                "int",
                                "sum",
                                "int", "int", "int", "int", "int", "int")),
                Arguments.of("<sun.misc.ExtensionDependency$1: java.util.jar.Manifest run()>",
                        new GraphMethod("sun.misc",
                                "ExtensionDependency$1",
                                "java.util.jar.Manifest",
                                "run")),
                Arguments.of("<java.lang.invoke.MethodHandleImpl$Intrinsic: void <clinit>()>",
                        new GraphMethod("java.lang.invoke",
                                "MethodHandleImpl$Intrinsic",
                                "void",
                                "<clinit>")),
                Arguments.of("<java.util.AbstractCollection: java.lang.Object[] finishToArray(java.lang.Object[],java.util.Iterator)>",
                        new GraphMethod("java.util",
                                "AbstractCollection",
                                "java.lang.Object[]",
                                "finishToArray",
                                "java.lang.Object[]", "java.util.Iterator")),
                Arguments.of("<java.util.concurrent.ConcurrentHashMap$TreeNode: java.util.concurrent.ConcurrentHashMap$TreeNode findTreeNode(int,java.lang.Object,java.lang.Class)>",
                        new GraphMethod("java.util.concurrent",
                                "ConcurrentHashMap$TreeNode",
                                "java.util.concurrent.ConcurrentHashMap$TreeNode",
                                "findTreeNode",
                                "int", "java.lang.Object", "java.lang.Class")),
                Arguments.of("<Driver_builtinTest0: void main(java.lang.String[])>",
                        new GraphMethod("",
                                "Driver_builtinTest0",
                                "void",
                                "main",
                                "java.lang.String[]")),
                Arguments.of("<java.lang.invoke.MethodHandles$Lookup: void <clinit>()>",
                        new GraphMethod("java.lang.invoke",
                                "MethodHandles$Lookup",
                                "void",
                                "<clinit>"))
        );
    }

    private static Stream<Arguments> provideGraphMethodToDescriptors() {
        return Stream.of(
                Arguments.of(new GraphMethod("boolean", new String[]{"java.lang.String", "java.lang.String[]"}),
                        "(java.lang.String,[Ljava.lang.String;)boolean"),
                Arguments.of(new GraphMethod("void", new String[]{"java.lang.String", "java.lang.String[]"}),
                        "(java.lang.String,[Ljava.lang.String;)void"),
                Arguments.of(new GraphMethod("void", new String[]{}),
                        "()void"),
                Arguments.of(new GraphMethod("int", new String[]{}),
                        "()int"),
                Arguments.of(new GraphMethod("java.lang.Object", new String[]{}),
                        "()java.lang.Object"),
                Arguments.of(new GraphMethod("", "", "void", "<init>"),
                        "()"),
                Arguments.of(new GraphMethod("", "", "void", "<init>", "boolean", "double"),
                        "(boolean,double)"),
                Arguments.of(new GraphMethod("boolean[]", new String[]{"java.lang.String", "int[]"}),
                        "(java.lang.String,[I)[Z"),
                Arguments.of(new GraphMethod("boolean[][]", new String[]{"java.lang.String", "int[][]"}),
                        "(java.lang.String,[[I)[[Z")
        );
    }

    @ParameterizedTest
    @MethodSource("provideMethodSignatureParsingStrings")
    public void methodSignatureParsing(String signature, GraphMethod clazz) {
        assertEquals(clazz, GraphMethod.of(signature));
    }

    @ParameterizedTest
    @MethodSource("provideGraphMethodToDescriptors")
    public void graphMethodToDescriptor(GraphMethod clazz, String descriptor) {
        assertEquals(descriptor, clazz.toTrackedInvocationsDescriptor());
    }
}
