package parsing.graph;

import nz.ac.vuw.ecs.hazlehjack.graph.CallGraphParser;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CallGraphParseTest {
    private static Stream<Arguments> provideSecondCsvColumn() {
        return Stream.of(
                Arguments.of("<register-finalize <sun.misc.ExtensionDependency$1: java.util.jar.Manifest run()>/<sun.misc.ExtensionDependency$1: java.util.jar.Manifest run()>/new java.util.jar.JarFile/0  >",
                        "<sun.misc.ExtensionDependency$1: java.util.jar.Manifest run()>"),
                Arguments.of("<java.lang.invoke.MethodHandleImpl$WrappedMember: java.lang.invoke.MethodHandle asTypeUncached(java.lang.invoke.MethodType)>/java.lang.invoke.MethodHandle.asType/0",
                        "<java.lang.invoke.MethodHandleImpl$WrappedMember: java.lang.invoke.MethodHandle asTypeUncached(java.lang.invoke.MethodType)>"),
                Arguments.of("<sun.text.normalizer.TrieIterator: boolean calculateNextBMPElement(sun.text.normalizer.RangeValueIterator$Element)>/sun.text.normalizer.TrieIterator.setResult/1",
                        "<sun.text.normalizer.TrieIterator: boolean calculateNextBMPElement(sun.text.normalizer.RangeValueIterator$Element)>"),
                Arguments.of("<calc.Calculator: int divide(int,int)>/calc.Calculator.subtract/2",
                        "<calc.Calculator: int divide(int,int)>"),
                Arguments.of("<register-finalize <java.lang.invoke.MethodHandleImpl$Intrinsic: void <clinit>()>/<java.lang.invoke.MethodHandleImpl$Intrinsic: void <clinit>()>/new java.lang.invoke.MethodHandleImpl$Intrinsic/0  >",
                        "<java.lang.invoke.MethodHandleImpl$Intrinsic: void <clinit>()>"),
                Arguments.of("<java.util.AbstractCollection: java.lang.Object[] finishToArray(java.lang.Object[],java.util.Iterator)>/java.util.Iterator.next/0",
                        "<java.util.AbstractCollection: java.lang.Object[] finishToArray(java.lang.Object[],java.util.Iterator)>"),
                Arguments.of("<java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$1: java.util.Map$Entry next()>/java.util.Iterator.next/0",
                        "<java.util.Collections$UnmodifiableMap$UnmodifiableEntrySet$1: java.util.Map$Entry next()>"),
                Arguments.of("<java.lang.invoke.MethodHandles$Lookup: void <clinit>()>/java.lang.invoke.MethodHandles.access$000/0",
                        "<java.lang.invoke.MethodHandles$Lookup: void <clinit>()>"),
                Arguments.of("<java.util.ServiceLoader: void access$100(java.lang.Class,java.lang.String,java.lang.Throwable)>/java.util.ServiceLoader.fail/0",
                        "<java.util.ServiceLoader: void access$100(java.lang.Class,java.lang.String,java.lang.Throwable)>"),
                Arguments.of("<abcTest: void checkBChildren()>/A.<init>/0",
                        "<abcTest: void checkBChildren()>"),
                Arguments.of("<sun.util.calendar.ZoneInfoFile: sun.util.calendar.ZoneInfo getZoneInfo(java.lang.String,long[],int[],long[],int[],sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule[])>/sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule.access$600/1",
                        "<sun.util.calendar.ZoneInfoFile: sun.util.calendar.ZoneInfo getZoneInfo(java.lang.String,long[],int[],long[],int[],sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule[])>")
        );
    }

    private static Stream<Arguments> provideFourthCsvColumn() {
        return Stream.of(
                Arguments.of("<calc.Calculator: boolean lessThan(int,int)>",
                        "<calc.Calculator: boolean lessThan(int,int)>"),
                Arguments.of("<java.lang.invoke.MethodHandle: java.lang.invoke.MethodHandle asType(java.lang.invoke.MethodType)>",
                        "<java.lang.invoke.MethodHandle: java.lang.invoke.MethodHandle asType(java.lang.invoke.MethodType)>"),
                Arguments.of("<A: void <init>()>",
                        "<A: void <init>()>"),
                Arguments.of("<sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule: int access$600(sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule)>",
                        "<sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule: int access$600(sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule)>")
        );
    }

    private static Stream<Arguments> provideDescriptorAndClass() {
        return Stream.of(
                Arguments.of("<calc.Calculator: boolean lessThan(int,int)>",
                        GraphMethod.of("calc", "Calculator", "boolean", "lessThan", "int", "int")),
                Arguments.of("<java.lang.invoke.MethodHandle: java.lang.invoke.MethodHandle asType(java.lang.invoke.MethodType)>",
                        GraphMethod.of("java.lang.invoke", "MethodHandle", "java.lang.invoke.MethodHandle", "asType", "java.lang.invoke.MethodType")),
                Arguments.of("<A: void <init>()>",
                        GraphMethod.of("", "A", "void", "<init>")),
                Arguments.of("<sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule: int access$600(sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule)>",
                        GraphMethod.of("sun.util.calendar", "ZoneInfoFile$ZoneOffsetTransitionRule", "int", "access$600", "sun.util.calendar.ZoneInfoFile$ZoneOffsetTransitionRule"))
        );
    }

    @ParameterizedTest
    @MethodSource("provideSecondCsvColumn")
    public void extractMethodSignatureSecondColumn(String column, String methodSignature) {
        assertEquals(methodSignature, CallGraphParser.extractMethodSignature(column));
    }

    @ParameterizedTest
    @MethodSource("provideFourthCsvColumn")
    public void extractMethodSignatureFourthColumn(String column, String methodSignature) {
        assertEquals(methodSignature, CallGraphParser.extractMethodSignature(column));
    }

    @ParameterizedTest
    @MethodSource("provideDescriptorAndClass")
    public void checkClassFromDescriptorIsCorrect(String descriptor, GraphMethod classToCheck) {
        assertEquals(classToCheck, GraphMethod.of(descriptor));
    }
}
