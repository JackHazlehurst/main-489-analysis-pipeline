package parsing;

import nz.ac.vuw.ecs.hazlehjack.graph.cha.Analyser;
import org.junit.jupiter.params.provider.Arguments;

import java.net.URL;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ParsingCommon {
    /**
     * Useful to make testing faster when debugging
     */
    private static boolean SKIP_ANALYSIS = false;
    /*
    No third party libraries
     */
    protected static Analyser analyser0;
    /*
    Same as 0 but with third party libraries
     */
    protected static Analyser analyser1;
    /*
    Two calls added to "checkNames" test from 1, third party libraries included
     */
    protected static Analyser analyser2;
    /*
    Same as 2, third party libraries removed
     */
    protected static Analyser analyser3;


    public ParsingCommon() {
        if (analyser0 == null && !SKIP_ANALYSIS) {
            URL abc0 = Thread.currentThread().getContextClassLoader().getResource("test-resources/abc-maven-0");
            analyser0 = new Analyser();
            analyser0.analyse(Paths.get(abc0.getPath()));
        }

        if (analyser1 == null && !SKIP_ANALYSIS && false) {
            URL abc1 = Thread.currentThread().getContextClassLoader().getResource("test-resources/abc-maven-1");
            analyser1 = new Analyser();
            analyser1.analyse(Paths.get(abc1.getPath()));
        }

        if (analyser2 == null && !SKIP_ANALYSIS && false) {
            URL abc2 = Thread.currentThread().getContextClassLoader().getResource("test-resources/abc-maven-2");
            analyser2 = new Analyser();
            analyser2.analyse(Paths.get(abc2.getPath()));
        }
        if (analyser3 == null && !SKIP_ANALYSIS) {
            URL abc3 = Thread.currentThread().getContextClassLoader().getResource("test-resources/abc-maven-3");
            analyser3 = new Analyser();
            analyser3.analyse(Paths.get(abc3.getPath()));
        }
    }

    public static Stream<Arguments> getAnalyserStream0() {
        return Stream.of(Arguments.of(analyser0));
    }

    public static Stream<Arguments> getAnalyserStream1() {
        return Stream.of(Arguments.of(analyser1));
    }

    public static Stream<Arguments> getAnalyserStream2() {
        return Stream.of(Arguments.of(analyser2));
    }

    public static Stream<Arguments> getAnalyserStream3() {
        return Stream.of(Arguments.of(analyser3));
    }
}
