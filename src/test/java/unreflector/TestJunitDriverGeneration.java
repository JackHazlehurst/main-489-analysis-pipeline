package unreflector;

import nz.ac.vuw.ecs.hazlehjack.unreflector.BuiltinTestDriverGenerator;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class TestJunitDriverGeneration {
    private static Path input;
    private static Path tmpOutput;

    @BeforeAll
    public static void setUp() {
        URL url = Thread.currentThread().getContextClassLoader().getResource("test-resources/unreflector/abc-maven-junit-4-5");
        input = Paths.get(url.getPath());

        tmpOutput = Paths.get(input.getParent().toString(), "test-tmp");
        new File(tmpOutput.toString()).mkdirs();
    }

    @Test
    public void driverGeneration() throws IOException {
        BuiltinTestDriverGenerator.main(new String[]{input.toString(), tmpOutput.toString()});

        long numJavaFiles = Files.walk(tmpOutput)
                .filter(p -> p.toString().endsWith(".java"))
                .count();
        assertEquals(3, numJavaFiles);

        Files.walk(tmpOutput)
                .filter(p -> p.toString().endsWith(".java"))
                .forEach(p -> {
                    try {
                        long numLines = Files.newBufferedReader(p)
                                .lines()
                                .count();

                        assertTrue(numLines == 6 || numLines == 18);
                    }
                    catch (IOException e) {
                        fail();
                    }
                });
        assertEquals(3, numJavaFiles);
    }

    @Test
    public void ensureMultipleRunsDoNotCauseAnException() {
        assertDoesNotThrow(() -> BuiltinTestDriverGenerator.main(new String[]{input.toString(), tmpOutput.toString()}));
        assertDoesNotThrow(() -> BuiltinTestDriverGenerator.main(new String[]{input.toString(), tmpOutput.toString()}));
    }

    @AfterAll
    public static void deleteTmpFiles() throws IOException {
        FileUtils.deleteDirectory(tmpOutput.toFile());
    }
}
