package analyser.graph;

import nz.ac.vuw.ecs.hazlehjack.graph.CallGraphParser;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.GraphUtils;
import nz.ac.vuw.ecs.hazlehjack.results.GraphAnalyser;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Paths;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestGraphAnalyser {
    private static GraphAnalyser graphAnalyser;
    private static Graph<GraphMethod, DefaultEdge> chaGraph;
    private static Graph<GraphMethod, DefaultEdge> doopGraph;

    @BeforeAll
    public static void performAnalysis() {
        // CHA Graph
        URL abc = Thread.currentThread().getContextClassLoader().getResource("test-resources/abc-maven-0");
        Graph<GraphMethod, DefaultEdge> graph;
        graph = new DefaultDirectedGraph<>(DefaultEdge.class);
        graph = CallGraphParser.parseCha(graph, Paths.get(abc.getPath()));
        graphAnalyser = new GraphAnalyser(graph);
        chaGraph = graph;

        // DOOP Graph
        URL callGraphPath = Thread.currentThread().getContextClassLoader().getResource("test-resources/call-graph-edges/CallGraphEdge.csv");
        doopGraph = CallGraphParser.parse(Paths.get(callGraphPath.getPath()));
    }

    @Test
    public void ensureEdgesCorrect() {
        assertEquals(95, graphAnalyser.getNumEdges());
    }

    @Test
    public void ensureVerticesCorrect() {
        assertEquals(34, graphAnalyser.getNumVertices());
    }

    @Test
    public void identifyTestsCorrectly() {
        Set<GraphMethod> methods = GraphUtils.getTestMethods(chaGraph);

        assertEquals(4, methods.size());
        hasMethodWithName(methods, "checkBChildren");
        hasMethodWithName(methods, "checkNotBChildren");
        hasMethodWithName(methods, "checkShoppingList");
        hasMethodWithName(methods, "checkNames");
    }

    @Test
    public void checkPositivityRateCalculations() {
        graphAnalyser.falseNegative(doopGraph);
        graphAnalyser.truePositives(doopGraph);
        graphAnalyser.falsePositives(doopGraph);
    }

    private void hasMethodWithName(Set<GraphMethod> methods, String methodName) {
        boolean doesMatch = methods.stream()
                .anyMatch(m -> m.methodName.equals(methodName));

        assertTrue(doesMatch);
    }
}
