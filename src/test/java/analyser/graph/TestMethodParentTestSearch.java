package analyser.graph;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.GraphUtils;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestMethodParentTestSearch {
    private static GraphMethod test0 = GraphMethod.of("testPkg", "TestClass", "void", "test0");
    private static GraphMethod test1 = GraphMethod.of("testPkg", "TestClass", "void", "test1");
    private static GraphMethod test2 = GraphMethod.of("testPkg", "TestClass", "void", "test2");
    private static GraphMethod test3 = GraphMethod.of("testPkg", "TestClass", "void", "test3");
    private static GraphMethod a = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "a", "int");
    private static GraphMethod b = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "b", "int");
    private static GraphMethod c = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "c", "int");
    private static GraphMethod d = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "d", "int");
    private static GraphMethod e = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "e", "int");
    private static GraphMethod f = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "f", "int");

    private static Graph<GraphMethod, DefaultEdge> testGraph = new DefaultDirectedGraph<>(DefaultEdge.class);
    private static Set<GraphMethod> testSet = new HashSet<>();

    @BeforeAll
    public static void createDummyCallGraph() {
        testGraph.addVertex(test0);
        testGraph.addVertex(test1);
        testGraph.addVertex(test2);
        testGraph.addVertex(test3);
        testGraph.addVertex(a);
        testGraph.addVertex(b);
        testGraph.addVertex(c);
        testGraph.addVertex(d);
        testGraph.addVertex(e);
        testGraph.addVertex(f);

        testGraph.addEdge(test2, test3);

        testGraph.addEdge(test0, f);
        testGraph.addEdge(test1, f);

        testGraph.addEdge(test1, a);
        testGraph.addEdge(test2, b);
        testGraph.addEdge(test3, c);
        testGraph.addEdge(a, b);
        testGraph.addEdge(b, c);
        testGraph.addEdge(d, c);
        testGraph.addEdge(d, d);// Recursive edge
        // Indirect recursion
        testGraph.addEdge(d, e);
        testGraph.addEdge(e, d);

        testSet.add(test0);
        testSet.add(test1);
        testSet.add(test2);
        testSet.add(test3);
    }

    @Test
    public void testAIsChanged() {
        ensureCorrectTestsFound(a, test1);
    }

    @Test
    public void testBIsChanged() {
        ensureCorrectTestsFound(b, test1, test2);
    }

    @Test
    public void testCIsChanged() {
        ensureCorrectTestsFound(c, test1, test2, test3);
    }

    @Test
    public void testFIsChanged() {
        ensureCorrectTestsFound(f, test0, test1);
    }

    @Test
    public void testMethodNoTestsReachable() {
        ensureCorrectTestsFound(d);
        ensureCorrectTestsFound(e);
    }

    @Test
    public void testTestAsChangedMethod() {
        ensureCorrectTestsFound(test0);
        ensureCorrectTestsFound(test1);
        ensureCorrectTestsFound(test2);
        ensureCorrectTestsFound(test3);
    }

    private void ensureCorrectTestsFound(GraphMethod toFind, GraphMethod... expectedTestsFound) {
        Set<GraphMethod> testsFound = GraphUtils.selectTestParents(testSet, testGraph, toFind);

        assertEquals(expectedTestsFound.length, testsFound.size());
        Arrays.stream(expectedTestsFound)
                .forEach( t -> assertTrue(testsFound.contains(t)));
    }
}
