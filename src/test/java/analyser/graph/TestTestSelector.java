package analyser.graph;

import nz.ac.vuw.ecs.hazlehjack.Util;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.GraphUtils;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestTestSelector {
    private static Set<String> testAnnotation = new HashSet<String>(Collections.singletonList("org.junit.jupiter.api.Test"));
    private static GraphMethod test0 = GraphMethod.of("testPkg", "TestClass", "void", "test0", testAnnotation);
    private static GraphMethod test1 = GraphMethod.of("testPkg", "TestClass", "void", "test1", testAnnotation);
    private static GraphMethod test2 = GraphMethod.of("testPkg", "TestClass", "void", "test2", testAnnotation);
    private static GraphMethod test3 = GraphMethod.of("testPkg", "TestClass", "void", "test3", testAnnotation);
    private static GraphMethod a = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "a", "int");
    private static GraphMethod b = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "b", "int");
    private static GraphMethod c = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "c", "int");
    private static GraphMethod d = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "d", "int");
    private static GraphMethod e = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "e", "int");
    private static GraphMethod f = GraphMethod.of("alpha", "Alphabet", "java.lang.String", "f", "int");

    private static Graph<GraphMethod, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);

    @BeforeAll
    public static void createDummyCallGraph() {
        graph.addVertex(test0);
        graph.addVertex(test1);
        graph.addVertex(test2);
        graph.addVertex(test3);
        graph.addVertex(a);
        graph.addVertex(b);
        graph.addVertex(c);
        graph.addVertex(d);
        graph.addVertex(e);
        graph.addVertex(f);

        graph.addEdge(test2, test3);

        graph.addEdge(test0, f);
        graph.addEdge(test1, f);

        graph.addEdge(test1, a);
        graph.addEdge(test2, b);
        graph.addEdge(test3, c);
        graph.addEdge(a, b);
        graph.addEdge(b, c);
        graph.addEdge(d, c);
        graph.addEdge(d, d);// Recursive edge
        // Indirect recursion
        graph.addEdge(d, e);
        graph.addEdge(e, d);
    }

    @Test
    public void testNothingSelectedWhenNoChangesPresent() {
        ensureExpectedTestsPresent(graph, Util.cloneGraph(graph));
    }

    @Test
    public void testNewMethodAdded() {
        Graph<GraphMethod, DefaultEdge> newGraph = Util.cloneGraph(graph);
        GraphMethod d6 = GraphMethod.of("alpha",
                "Numeric",
                "java.lang.Double",
                "d6",
                "float");
        newGraph.addVertex(d6);
        newGraph.addEdge(test1, d6);

        ensureExpectedTestsPresent(graph, newGraph, test1);
    }

    @Test
    public void testNewMethodAddedIndirect() {
        Graph<GraphMethod, DefaultEdge> newGraph = Util.cloneGraph(graph);
        GraphMethod d6 = GraphMethod.of("alpha",
                "Numeric",
                "java.lang.Double",
                "d6",
                "float");
        newGraph.addVertex(d6);
        newGraph.addEdge(a, d6);

        ensureExpectedTestsPresent(graph, newGraph, test1);
    }

    @Test
    public void testNewMethodAddedEffectsMultipleTests() {
        Graph<GraphMethod, DefaultEdge> newGraph = Util.cloneGraph(graph);
        GraphMethod d7 = GraphMethod.of("alpha",
                "Numeric",
                "java.lang.Double",
                "d7",
                "float");
        newGraph.addVertex(d7);
        newGraph.addEdge(c, d7);

        ensureExpectedTestsPresent(graph, newGraph, test1, test2, test3);
    }

    @Test
    public void testDeletedMethod() {
        Graph<GraphMethod, DefaultEdge> newGraph = Util.cloneGraph(graph);
        newGraph.removeVertex(f);

        ensureExpectedTestsPresent(graph, newGraph, test0, test1);
    }

    @Test
    public void testNewInvocationOfExistingMethod() {
        Graph<GraphMethod, DefaultEdge> newGraph = Util.cloneGraph(graph);
        newGraph.addEdge(b, a);

        ensureExpectedTestsPresent(graph, newGraph, test2);
    }

    @Test
    public void addNewTest() {
        Graph<GraphMethod, DefaultEdge> newGraph = Util.cloneGraph(graph);
        GraphMethod testTest = GraphMethod.of("testPkg",
                "TestClass",
                "void",
                "testTest",
                testAnnotation);
        newGraph.addVertex(testTest);
        newGraph.addEdge(testTest, f);

        ensureExpectedTestsPresent(graph, newGraph, testTest);
    }

    private void ensureExpectedTestsPresent(Graph<GraphMethod, DefaultEdge> v1,
                                            Graph<GraphMethod, DefaultEdge> v2,
                                            GraphMethod... expectedTests) {
        Set<GraphMethod> selectedTests = GraphUtils.selectTests(v1, v2);

        assertEquals(expectedTests.length, selectedTests.size());
        Arrays.stream(expectedTests)
                .forEach(t -> assertTrue(selectedTests.contains(t)));
    }
}
