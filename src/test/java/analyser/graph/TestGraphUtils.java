package analyser.graph;

import nz.ac.vuw.ecs.hazlehjack.graph.CallGraphParser;
import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.graph.utils.GraphUtils;
import nz.ac.vuw.ecs.hazlehjack.unreflector.BuiltinTestDriverGenerator;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultUndirectedGraph;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestGraphUtils {
    private static Graph<GraphMethod, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);
    private static Path callGraphEdgePath;
    private static Path input;
    private static Path tmpOutput;

    @BeforeAll
    public static void setUp() {
        // Static Analysis
        URL urlClasses = Thread.currentThread().getContextClassLoader().getResource("test-resources/testsWithPackages/classes");
        URL callGraph = Thread.currentThread().getContextClassLoader().getResource("test-resources/testsWithPackages/CallGraphEdgeTestWithPackage.csv");
        input = Paths.get(urlClasses.getPath());
        callGraphEdgePath = Paths.get(callGraph.getPath());

        tmpOutput = Paths.get(input.getParent().toString(), "tests-with-packages-tmp");
        new File(tmpOutput.toString()).mkdirs();

        // CHA Analysis
        URL abc = Thread.currentThread().getContextClassLoader().getResource("test-resources/testsWithPackages/classes");
        CallGraphParser.parseCha(graph, Paths.get(abc.getPath()));
    }

    @Test
    public void testGetAllChildrenWorks() {
        Set<GraphMethod> tests = GraphUtils.getTestMethods(graph);
        Set<GraphMethod> children = GraphUtils.getAllChildren(graph, tests);
        assertEquals(57, children.size());
    }

    @Test
    public void testToCsvForChaAnalysisWorks() throws IOException {
        Path csvPath = Paths.get("target/test-output.csv");
        GraphUtils.testsToCsv(graph, csvPath);

        checkLines(csvPath, 92);
        checkHeader(csvPath);
    }

    @Test
    public void testStaticAnalysisIdentifiesTestsCorrectly() throws IOException {
        Set<GraphMethod> tests = BuiltinTestDriverGenerator.main(new String[]{input.toString(), tmpOutput.toString()});

        assertEquals(8, tests.size());
        assertTrue(tests.contains(GraphMethod.of("", "abcTest", "void", "checkBChildren")));
        assertTrue(tests.contains(GraphMethod.of("", "abcTest", "void", "checkNotBChildren")));
        assertTrue(tests.contains(GraphMethod.of("", "abcTest", "void", "checkShoppingList")));
        assertTrue(tests.contains(GraphMethod.of("", "abcTest", "void", "checkNames")));
        assertTrue(tests.contains(GraphMethod.of("", "MoreTests", "void", "testE")));
        assertTrue(tests.contains(GraphMethod.of("", "MoreTests", "void", "testNumberOfAlphabet")));
        assertTrue(tests.contains(GraphMethod.of("", "MoreTests", "void", "testNameAgainstRegex")));
        assertTrue(tests.contains(GraphMethod.of("testPkg", "TestsInPackage", "void", "testDoAlphabetDoesNotThrow")));
    }

    @Test
    public void testToCsvForStaticAnalysisWorks() throws IOException {
        Path csvPath = Paths.get("target/test-static-output.csv");
        Set<GraphMethod> tests = BuiltinTestDriverGenerator.main(new String[]{input.toString(), tmpOutput.toString()});
        Graph<GraphMethod, DefaultEdge> staticGraph = CallGraphParser.parse(callGraphEdgePath);
        GraphUtils.testsToCsv(staticGraph, csvPath, tests);

        checkLines(csvPath, 6979);
        checkHeader(csvPath);
    }

    private void checkLines(Path csvPath, long expectedLines) throws IOException {
        long lines = Files.newBufferedReader(csvPath)
                .lines()
                .count();
        assertEquals(expectedLines, lines);
    }

    private void checkHeader(Path csvPath) throws IOException {
        Files.newBufferedReader(csvPath)
                .lines()
                .limit(1)
                .forEach(l -> {
                    String[] headers = l.split("\t");
                    assertEquals("testClass", headers[0]);
                    assertEquals("testMethod", headers[1]);
                    assertEquals("class", headers[2]);
                    assertEquals("name", headers[3]);
                    assertEquals("descriptor", headers[4]);
                });
    }
}
