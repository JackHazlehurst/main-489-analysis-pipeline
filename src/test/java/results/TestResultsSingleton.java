package results;

import nz.ac.vuw.ecs.hazlehjack.graph.GraphMethod;
import nz.ac.vuw.ecs.hazlehjack.results.Results;
import nz.ac.vuw.ecs.hazlehjack.results.VersionResult;
import nz.ac.vuw.ecs.hazlehjack.unreflector.StaticAnalysisMain;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestResultsSingleton {
    public final String HASH_1 = "aa6813e7c75b31a1e24844d9a06af204f2c39e60";
    public final String HASH_2 = "bb5a4e33553a5f098ab6065a54e1ae7985025d26";
    public final String HASH_3 = "cc4b645f173911b85cb9f4c4affcef728b7e1564";
    public final String HASH_4 = "ddc86f78689219227236fed63553c58eebf358ef";
    public static Path classFiles;

    @BeforeAll
    public static void setUp() {
        URL url = Thread.currentThread().getContextClassLoader().getResource("test-resources/abc-maven-0");
        classFiles = Paths.get(url.getPath());
    }

    @BeforeEach
    public void clearResults() {
        Results.clear();
    }

    @Test
    public void testIndexing() {
        Results.createResultRecord(HASH_1);
        Results.createResultRecord(HASH_2);
        Results.createResultRecord(HASH_3);
        Results.createResultRecord(HASH_4);

        assertEquals(0, Results.commitHashToIndex(HASH_1));
        assertEquals(1, Results.commitHashToIndex(HASH_2));
        assertEquals(2, Results.commitHashToIndex(HASH_3));
        assertEquals(3, Results.commitHashToIndex(HASH_4));
    }

    @Test
    public void ensurePreviousVersionWorksOnFirstElement() {
        Results.createResultRecord(HASH_1);
        Results.createResultRecord(HASH_2);
        Results.createResultRecord(HASH_3);
        Results.createResultRecord(HASH_4);

        Graph<GraphMethod, DefaultEdge> graph = Results.getPreviousVersionResultOrEmpty(VersionResult.of(HASH_1), g -> g.chaAnalysis.result);
        assertTrue(graph.edgeSet().isEmpty());
        assertTrue(graph.vertexSet().isEmpty());
    }

    @Test
    public void ensurePreviousVersionWorksOnSecondElementNonEmpty() {
        Results.createResultRecord(HASH_1);
        Results.createResultRecord(HASH_2);
        Results.createResultRecord(HASH_3);
        Results.createResultRecord(HASH_4);


        Results.recordChaAnalysis(HASH_1, classFiles);

        Graph<GraphMethod, DefaultEdge> graph = Results.getPreviousVersionResultOrEmpty(VersionResult.of(HASH_1), g -> g.chaAnalysis.result);
        assertTrue(!graph.edgeSet().isEmpty());
        assertTrue(!graph.vertexSet().isEmpty());
    }
}
