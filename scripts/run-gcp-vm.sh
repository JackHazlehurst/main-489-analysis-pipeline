#!/bin/bash
sudo apt update
sudo apt upgrade -y
sudo apt install maven -y

sudo apt install openjdk-8-source -y
sudo update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java

sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo apt-get install docker-ce=5:19.03.12~3-0~ubuntu-focal docker-ce-cli=5:19.03.12~3-0~ubuntu-focal containerd.io -y

# Pulling Docker image now so this does not affect timing when running analysis
sudo docker pull gfour/doop:4.21.6

sudo mkdir /data
sudo chmod 777 /data
cd /data

git clone https://JackHazlehurst@bitbucket.org/JackHazlehurst/ground-truth-recorder.git
git clone https://JackHazlehurst@bitbucket.org/JackHazlehurst/auto-ground-truth-recorder.git

cd /data/ground-truth-recorder
sudo mvn install

cd /data/auto-ground-truth-recorder
export MAVEN_OPTS="-Xms32000m -Xmx32000m"
sudo mvn -Dmaven.test.skip=true install
mkdir /data/auto-ground-truth-recorder/repos
sudo mvn exec:java -Dexec.mainClass="nz.ac.vuw.ecs.hazlehjack.GroundTruthRecorder" -Dexec.args="https://JackHazlehurst@bitbucket.org/JackHazlehurst/abc-maven.git /data/auto-ground-truth-recorder/repos 8d632dfd66a62ebaa9a95819c660bcd2a2251aaa 3" -Dexec.cleanupDaemonThreads=false